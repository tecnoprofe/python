import pygame
import sys

# Inicializa pygame
pygame.init()

# Configuraciones iniciales
WIDTH, HEIGHT = 800, 600
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption('Temporizador Pygame')

# Colores
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Crea una fuente
font = pygame.font.Font(None, 74)

def display_timer(seconds_left, screen, font):
    """Muestra el temporizador en pantalla."""
    mins, secs = divmod(seconds_left, 60)
    timeformat = '{:02d}:{:02d}'.format(mins, secs)
    text = font.render(timeformat, True, BLACK)
    screen.blit(text, (WIDTH // 2 - text.get_width() // 2, HEIGHT // 2 - text.get_height() // 2))

# Temporizador
start_time = 10 * 1  # 10 minutos en segundos
current_time = start_time

clock = pygame.time.Clock()

while True:
    screen.fill(WHITE)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    # Actualizar temporizador
    display_timer(current_time, screen, font)
    current_time -= 1
    if current_time < 0:
        current_time = 0

    pygame.display.flip()
    clock.tick(1)  # Asegura que el loop se ejecute una vez por segundo

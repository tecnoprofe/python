#   Proyecto creado por TecnoProfe
#    youtube: https://www.youtube.com/tecnoprofe
import pygame
import random

ANCHO = 800
ALTO = 600
DIMENSIONES=(ANCHO,ALTO)
# ====================================
# Colores
NEGRO = (0, 0, 0)
BLANCO = (152, 255, 255)

# ====================================
# Configuraciones iniciales.
pygame.init()
ventana = pygame.display.set_mode(DIMENSIONES)
pygame.display.set_caption("Serpiente v1")
reloj = pygame.time.Clock()

#Generación de la comida de la serpiente
x=random.randrange(1, ANCHO-30)
y=random.randrange(1, ALTO-30)
comida=pygame.Rect(x, y, 30, 30)

#Generación de la serpiente
serpiente=pygame.Rect(400, 300, 30, 30)

#paredes
pared_izquierda=pygame.Rect(0, 0, 10, 600)
pared_arriba=pygame.Rect(0, 0,800,10 )
pared_abajo=pygame.Rect(0, 590, 800, 600)
pared_derecha=pygame.Rect(790, 0, 800, 600)

#variables generales
velocidad=10
direccion="sin direccion"
vida=3
comidas=0
fuente = pygame.font.Font('freesansbold.ttf', 32)    
texto_salir=""
# ========================================
# Juego Bucle
while True:
    # velocidad sujerida del juego
    reloj.tick(velocidad)
    # obtiene eventos de entrada
    for event in pygame.event.get():
        # Verifica si presionó el boton cerrar. 
        if event.type == pygame.QUIT:
            quit()

        tecla = pygame.key.get_pressed()   
        if tecla[pygame.K_LEFT]:
            direccion="izquierda"            
        if tecla[pygame.K_UP]:
            direccion="arriba"            
        if tecla[pygame.K_RIGHT]:
            direccion="derecha"            
        if tecla[pygame.K_DOWN]:
            direccion="abajo"  
        if tecla[pygame.K_RETURN]:
            vida=3
            comidas=0
            velocidad=10
    
    #LOGICA 
    #==============================

    # Control de botones
    if direccion=="izquierda":
        serpiente.left-=30         
    elif direccion=="derecha":
        serpiente.left+=30        
    if direccion=="arriba":
        serpiente.top-=30        
    if direccion=="abajo":
        serpiente.top+=30 
    
    # colision para la comida
    if serpiente.colliderect(comida):
        comidas+=1        
        x=random.randrange(1, ANCHO-30)
        y=random.randrange(1, ALTO-30)
        comida.left=x
        comida.top=y
        serpiente.width+=3
        serpiente.height+=3
        velocidad+=2
    
    # colision para la comida
    if serpiente.colliderect(pared_izquierda):
        vida-=1
        serpiente.left=400
        serpiente.top=300
    if serpiente.colliderect(pared_derecha):
        vida-=1
        serpiente.left=400
        serpiente.top=300
    if serpiente.colliderect(pared_arriba):
        vida-=1
        serpiente.left=400
        serpiente.top=300
    if serpiente.colliderect(pared_abajo):
        vida-=1
        serpiente.left=400
        serpiente.top=300
    
   
    



    # DIBUJAR
    # ==============================
    #Renderiza fondo negro
    ventana.fill(NEGRO)    
    #Dibujar comida
    pygame.draw.rect(ventana, (200,65,20), comida)    
    # Dibujar serpiente
    pygame.draw.rect(ventana, (0,180,30),serpiente )       
    # Dibujando paredes
    pygame.draw.rect(ventana, (158,180,50),pared_izquierda)
    pygame.draw.rect(ventana, (158,180,50),pared_arriba)
    pygame.draw.rect(ventana, (158,180,50),pared_derecha)
    pygame.draw.rect(ventana, (158,180,50),pared_abajo)
    
    texto_comidas = fuente.render('Comidas= '+str(comidas), True, (0,255,0), (0,45,4))
    ventana.blit(texto_comidas,(0,0)) 

    texto_vida = fuente.render('Vida= '+str(vida), True, (0,255,0), (0,45,4))
    ventana.blit(texto_vida,(650,0)) 
    if vida==0:
        GameOver = fuente.render('GAME OVER.', True, (0,255,0), (0,45,4))
        ventana.blit(GameOver,(350,280)) 

        GameOver = fuente.render('Enter, para volver a intentar.', True, (0,255,0), (0,45,4))
        ventana.blit(GameOver,(240,310)) 

        serpiente.left=300
        serpiente.top=300
    
    pygame.display.flip()
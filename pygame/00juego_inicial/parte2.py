import pygame
import math

ROJO = (255, 0, 0)
AZUL = (0, 0, 255)
VERDE = (0, 255, 0)
NEGRO = (0, 0, 0)
MARRON= (200, 2, 3)

pygame.init()
dimensiones = [500, 600]
pantalla = pygame.display.set_mode(dimensiones)
pygame.display.set_caption("-------- Animación ------")
juego_terminado = False
reloj = pygame.time.Clock()
t = 0
def puntosCanion(ab, angulo, radio1, radio2):
    puntos = []
    puntos.append(xyCircunferencia(angulo, ab, radio1))
    puntos.append(xyCircunferencia(angulo, ab, radio2))
    return puntos

def xyCircunferencia(t, centro, radio):
    t = math.radians(t)
    x = radio * math.cos(t) + centro[0]
    y = radio * math.sin(t) + centro[1]
    return [round(x), round(y)]

def dibujaCanion(pantalla, xy, angulo):
    x, y = xy
    CoordCanion = puntosCanion(xy, angulo, 30, 18)
    pygame.draw.circle(pantalla, MARRON, xy, 20)
    pygame.draw.rect(pantalla, NEGRO, [x - 30, y, 60, 40])
    pygame.draw.line(pantalla, MARRON, CoordCanion[0], CoordCanion[1], 6)
    return CoordCanion[0]
def main():
    pygame.init()
    pantalla = pygame.display.set_mode(dimensiones)
    pygame.display.set_caption("-------- Animación ------")
    juego_terminado = False
    reloj = pygame.time.Clock()
    a = 185
    av = 1
    xv = 1
    yv = 1
    y, x = [1, 1]
    disparo = False
    while juego_terminado is False:
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                juego_terminado = True
        a += av
        if a > 355 or a < 185:
            av *= -1
        pantalla.fill(AZUL)
        dibujaCanion(pantalla, [200, dimensiones[1] - 10], a)
        if a == 290:
            CoordCanion = puntosCanion([200, dimensiones[1] - 10], a, 30, 18)
            x, y = CoordCanion[0]
            disparo = True
        if a == 185:
            disparo = False
        if x > 395 or x < 5:
            xv *= -1
        if y > 295 or y < 5:
            yv *= -1
        if disparo:
            pygame.draw.circle(pantalla, (255, 0, 0), [x, y], 4)
            x += xv
            y -= yv
        pygame.display.flip()
        reloj.tick(20)
    pygame.quit()


if __name__ == "__main__":
    main()
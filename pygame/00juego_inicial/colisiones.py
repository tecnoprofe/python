import pygame
import sys
from shapely.geometry import Polygon

# Inicializar pygame
pygame.init()

# Configuración de la pantalla
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Colisiones con Polígonos en Pygame usando Shapely")

# Colores
WHITE = (255, 255, 255)
BLUE = (0, 128, 255)
RED = (255, 0, 0)

# Reloj para controlar los FPS
clock = pygame.time.Clock()
fps = 60

# Posición inicial de los polígonos
poly1_x = 200
poly1_y = 300
poly2_x = 500
poly2_y = 300
poly_speed = 5

# Definir los puntos de los polígonos
poly1_points = [
    (0, -50),
    (50, 50),
    (-50, 50)
]
poly2_points = [
    (0, -40),
    (40, 40),
    (-40, 40)
]

def move_polygon(points, dx, dy):
    return [(x + dx, y + dy) for x, y in points]

# Bucle principal del juego
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Obtener el estado actual de todas las teclas
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        poly1_x -= poly_speed
    if keys[pygame.K_RIGHT]:
        poly1_x += poly_speed
    if keys[pygame.K_UP]:
        poly1_y -= poly_speed
    if keys[pygame.K_DOWN]:
        poly1_y += poly_speed

    # Mover los polígonos a la nueva posición
    poly1_points_moved = move_polygon(poly1_points, poly1_x, poly1_y)
    poly2_points_moved = move_polygon(poly2_points, poly2_x, poly2_y)

    # Crear polígonos de shapely
    poly1 = Polygon(poly1_points_moved)
    poly2 = Polygon(poly2_points_moved)

    # Detectar colisiones
    collision = poly1.intersects(poly2)

    # Llenar la pantalla de blanco
    screen.fill(WHITE)

    # Dibujar los polígonos
    pygame.draw.polygon(screen, BLUE, poly1_points_moved)
    pygame.draw.polygon(screen, RED, poly2_points_moved)

    # Mostrar si hay colisión
    if collision:
        font = pygame.font.Font(None, 36)
        text_surface = font.render("Colisión Detectada!", False, RED)
        screen.blit(text_surface, (100, 100))

    # Actualizar la pantalla
    pygame.display.flip()

    # Controlar los FPS
    clock.tick(fps)

# Salir de pygame
pygame.quit()
sys.exit()

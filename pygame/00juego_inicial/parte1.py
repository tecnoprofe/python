import math
import random
import pygame
import sys
import os
from shapely.geometry import Polygon
#VARIABLES
#---------------------------------------------
# con este comando se inicia pygame
pygame.init()
#iniciar una pantalla
ventana= pygame.display.set_mode((800,600))
#Reloj del juego

# Obtener la ruta del directorio del script
base_path = os.path.dirname(os.path.abspath(__file__))

# Construir la ruta completa a la imagen de fondo01
fondo_path01 = os.path.join(base_path, "fondo01.jpg")


# Construir la ruta completa a la imagen de fondo00
fondo_path00 = os.path.join(base_path, "fondo.jpg")


fondo=pygame.image.load(fondo_path00)


reloj=pygame.time.Clock()
fps=70
#Colores
Rojo=(255,0,0)

Verde=(0,255,0)

x=400
y=550

x1=400
y1=550

coloraleatorio=(0,0,255)

nave_speed = 5
balas=[]
bala_speed = 10


Puntos_Corazon=[
        (100,100),
        (200,100),
        (300,200),
        (400,100),
        (500,100),
        (600,200),
        (500,500),
        (300,600),
        (100,500),
        (0,200),
        ]

triangle_points=[
    (-10,0),
    (0,18),
    (10,0),    
]

triangle_points2=[
    (-10,0),
    (0,18),
    (10,0),    
]

# Creacion de fuente de letra       ] 
fuente=pygame.font.Font(None,45)

# variable de la vida del juagador
vidas=2000
#FUNCIONES
#---------------------------------------------
def hexagon_points(size):
    hexagon=[
        (size * 0.5, 0),
        (size * 1.5, 0),
        (size * 2, size * 0.866),
        (size * 1.5, size * 1.732),
        (size * 0.5, size * 1.732),
        (0, size * 0.866)
    ]
    return hexagon

def octagon_points(size):
    octagon=[
        (size * 0.707, 0),
        (size * 1.707, 0),
        (size * 2.414, size * 0.707),
        (size * 2.414, size * 1.707),
        (size * 1.707, size * 2.414),
        (size * 0.707, size * 2.414),
        (0, size * 1.707),
        (0, size * 0.707)
    ]
    return octagon



def escaladoPUntos(Puntos,escalado):
    nuevos_puntos=[]
    for x,y in Puntos:
        nuevos_puntos.append((x*escalado,y*escalado))
    return nuevos_puntos

def moverpuntos(inc_x,inc_y,Puntos):
    nuevos_puntos=[]
    for x,y in Puntos:
        nuevos_puntos.append((x+inc_x,y+inc_y))        
    return nuevos_puntos




#funciones
def mickey(x,y):
    pygame.draw.rect(ventana,coloraleatorio,(x,y,50,50))
    pygame.draw.circle(ventana,coloraleatorio,(x,y),20)
    pygame.draw.circle(ventana,coloraleatorio,(x+40,y),20)

def nave(base,x,y):
    puntos=[
        (0+x, -20+y),  # Punta de la nave
        (-15+x, 20+y),  # Esquina inferior izquierda
        (0+x, 10+y),  # Centro inferior
        (15+x, 20+y)  # Esquina inferior derecha
        ]    
    pygame.draw.polygon(base,coloraleatorio,puntos)


def corazon(surface, x, y, size):
    points = []
    for i in range(0, 360, 1):
        angle = math.radians(i)
        px = x + size * (16 * math.sin(angle) ** 3)
        py = y - size * (13 * math.cos(angle) - 5 * math.cos(2 * angle) - 2 * math.cos(3 * angle) - math.cos(4 * angle))
        points.append((px, py))
    pygame.draw.polygon(surface, (255,0,0), points)

def corazon2(Puntos,pintura,surface):
    pygame.draw.polygon(surface,pintura,Puntos)
    
def muros(base,color):
    pygame.draw.rect(base,color,(0,0,800,10))
    pygame.draw.rect(base,color,(0,400,800,10))
    pygame.draw.rect(base,color,(600,300,10,400))


#BUCLE PRINCIPAL
#---------------------------------------------
while True:
    for evento in pygame.event.get():
        if evento.type==pygame.QUIT:
            print("Adios. ")
            sys.exit()
            
        elif evento.type == pygame.KEYDOWN:

            if evento.key == pygame.K_SPACE:
                # Disparar una bala desde la punta de la nave
                bala_x = x
                bala_y = y - 20
                balas.append((bala_x, bala_y))

    #CONTROLES DE TECLADO
    #---------------------------------------------
    teclas= pygame.key.get_pressed()
    if teclas[pygame.K_LEFT]:
        x=x-1
    if teclas[pygame.K_RIGHT]:
        x=x+1

    if teclas[pygame.K_UP]:
        y=y-1
    if teclas[pygame.K_DOWN]:
        y=y+1



    if teclas[pygame.K_SPACE]:
        coloraleatorio=(random.randint(0,255),random.randint(0,255),random.randint(0,255))
        fondo=pygame.image.load(fondo_path01)
        
    

    # LOGICA
    #---------------------------------------------
    triangle=moverpuntos(x,y,triangle_points)
    triangle1=moverpuntos(x1,y1,triangle_points2)

    poly1 =Polygon(triangle)
    poly2 =Polygon(triangle1)

    collision=poly1.intersects(poly2)
    
    textoGameOver=fuente.render("ADELANTE",True,(233,200,33))

    if collision==True:
        vidas=vidas-1
    if vidas<=0:
        textoGameOver=fuente.render("GAME OVER",True,(233,200,33))
        vidas=0
    


    balas = [(x, y - bala_speed) for x, y in balas if y > 0]


    #DIBUJAR
    #---------------------------------------------
    

    ventana.fill((0,0,103))
    ventana.blit(fondo,(0,0))  
    muros(ventana,Verde)

    nave(ventana,x,y)

    texto0= fuente.render("Vida="+str(vidas),True,(233,200,33))
    ventana.blit(texto0,(10,10))

    texto1= fuente.render("UPDS GAME",True,(233,200,33))
    ventana.blit(texto1,(600,10))    

    n_puntos=escaladoPUntos(Puntos_Corazon,0.1)            

    

    randomsize=random.randint(10,50)
    x_piedrahex=random.randint(0,800)
    y_piedrahex=0
    nuevos_points=moverpuntos(x_piedrahex,y_piedrahex,hexagon_points(randomsize)) 
    pygame.draw.polygon(ventana,Verde,nuevos_points)

    

    pygame.draw.polygon(ventana,Verde,triangle)
    pygame.draw.polygon(ventana,Verde,triangle1)
    
    ventana.blit(textoGameOver,(600,400))    


 # Dibujar las balas
    for bala in balas:
        pygame.draw.circle(ventana, Rojo, bala, 5)

    pygame.display.flip()
    reloj.tick(fps)
pygame.quit()
sys.exit()
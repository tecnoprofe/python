import pygame   # Importar la librería Pygame
import os       # Importar la librería para manejo del Sistema Operativo

ANCHO= 800
ALTO= 600

COLOR = pygame.Color('white') #Color de fondo
FPS = 80 #Frames por segundo
 

class Sprite(pygame.sprite.Sprite):
    def __init__(self,x,y):
        super().__init__()

        self.imagenes = []   # crea una lista.                
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk1.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk2.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk3.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk4.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk5.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk6.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk7.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk8.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk9.png'))
        self.imagenes.append(pygame.image.load(os.path.dirname(__file__)+'\\img\\walk\\walk10.png'))

        self.index = 0
        
        self.x=x
        self.y=y
        self.image = self.imagenes[self.index] 
        self.rect = pygame.Rect(0, 0, 150, 198)
 
    def update(self):        
        #Control de teclas cursoras    
        tecla = pygame.key.get_pressed()   
        if tecla[pygame.K_LEFT]:            
            if self.index < 0:                
                self.index = 10                    
            self.index -= 1
            self.image = self.imagenes[self.index]            
            self.rect.x-=3
            print("index: " ,self.index,"  len imagenes:",len(self.imagenes))

        if tecla[pygame.K_RIGHT]:                                               
            if self.index > len(self.imagenes)-1:
                self.index = 0                    
            self.image = self.imagenes[self.index]
            self.index +=1
            self.rect.x+=3
            print("index: " ,self.index,"  len imagenes:",len(self.imagenes))
            

    
    
        
 

pygame.init()   # iniciar pygame
ventana = pygame.display.set_mode((ANCHO,ALTO))   # Tamaño de la ventana

# ========================================
# Declaración de grupos de sprite's
todos_los_sprites = pygame.sprite.Group()
# Declaración de objetos
geronimo = Sprite(100,100)
todos_los_sprites.add(geronimo)

clock = pygame.time.Clock()

while True:
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            quit()

    # Tiempo 
    clock.tick(7)
    
    #Renderiza fondo 
    ventana.fill(COLOR)
    # dibujando todo
    todos_los_sprites.draw(ventana)
    # Actualiza los sprite
    todos_los_sprites.update()
    # Actualizar las animaciones.
    pygame.display.flip()    
    
                           
      
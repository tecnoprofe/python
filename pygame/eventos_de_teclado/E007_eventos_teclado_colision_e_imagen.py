import pygame
import os

pygame.init()
ventana=pygame.display.set_mode((800,600))
bandera=True

velocidad_normal = 1
velocidad_rapida = 3

fondo=pygame.image.load(os.path.dirname(__file__)+"\\img\\fondo02.jpeg")

apple=pygame.image.load(os.path.dirname(__file__)+"\\img\\apple.png")
apple1=apple.get_rect()

cuadrado=pygame.Rect(100,100,100,100)
muroizq=pygame.Rect(0,0,20,600)
muroder=pygame.Rect(780,0,20,600)
murocentral = pygame.Rect(200,300,400,20)
while bandera==True:
    for evento in pygame.event.get():
        if evento.type== pygame.QUIT:
            bandera=False

    # Guardar la posición anterior del jugador (antes del movimiento)
    cuadrado_previo = cuadrado.copy()

    # Determinar la velocidad en función de si la tecla Ctrl está presionada
    tecla_presionada=pygame.key.get_pressed()
    if tecla_presionada[pygame.K_LCTRL] or tecla_presionada[pygame.K_RCTRL]:
        velocidad = velocidad_rapida
    else:
        velocidad = velocidad_normal
    
    # Mover el jugador
    if tecla_presionada[pygame.K_LEFT]:
        cuadrado.x -= velocidad
    if tecla_presionada[pygame.K_RIGHT]:
        cuadrado.x += velocidad
    if tecla_presionada[pygame.K_UP]:
        cuadrado.y -= velocidad
    if tecla_presionada[pygame.K_DOWN]:
        cuadrado.y += velocidad


    if cuadrado.colliderect(muroizq):
        cuadrado=cuadrado_previo
    
    if cuadrado.colliderect(muroder):
        cuadrado=cuadrado_previo    
    
    if cuadrado.colliderect(murocentral):
        cuadrado=cuadrado_previo    

    #DIBUJO DE OBJETOS
    ventana.fill((150,130,220))
    #Renderiza fondo noegro
    ventana.blit(fondo,(0,0))
    ventana.blit(apple,cuadrado)

    pygame.draw.rect(ventana,(120,120,140),muroizq)
    pygame.draw.rect(ventana,(120,120,140),muroder)
   
    pygame.draw.rect(ventana,(120,120,140),murocentral)
    pygame.draw.rect(ventana,(120,30,240),cuadrado)

    pygame.display.update()
pygame.quit()

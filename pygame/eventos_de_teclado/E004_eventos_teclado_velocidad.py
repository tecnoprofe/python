import pygame
pygame.init()
ventana=pygame.display.set_mode((800,600))
bandera=True

velocidad_normal = 1
velocidad_rapida = 2

cuadrado=pygame.Rect(100,100,100,100)
muroizq=pygame.Rect(0,0,20,600)
muroder=pygame.Rect(780,0,20,600)
while bandera==True:
    for evento in pygame.event.get():
        if evento.type== pygame.QUIT:
            bandera=False

    # Determinar la velocidad en función de si la tecla Ctrl está presionada
    tecla_presionada=pygame.key.get_pressed()
    if tecla_presionada[pygame.K_LCTRL] or tecla_presionada[pygame.K_RCTRL]:
        velocidad = velocidad_rapida
    else:
        velocidad = velocidad_normal
    
    # Mover el jugador
    if tecla_presionada[pygame.K_LEFT]:
        cuadrado.x -= velocidad # cuadrado.x = cuadrado.x - velocidad
    if tecla_presionada[pygame.K_RIGHT]:
        cuadrado.x += velocidad
    if tecla_presionada[pygame.K_UP]:
        cuadrado.y -= velocidad
    if tecla_presionada[pygame.K_DOWN]:
        cuadrado.y += velocidad


    if cuadrado.colliderect(muroizq):
        color=(230,0,0)
    else:
        color=(120,120,140)

    if cuadrado.colliderect(muroder):
        color=(230,0,0)
    else:
        color=(120,120,140)


    #DIBUJO DE OBJETOS
    ventana.fill((50,30,180))
    pygame.draw.rect(ventana,(120,120,140),muroizq)
    pygame.draw.rect(ventana,(120,120,140),muroder)
    pygame.draw.rect(ventana,color,cuadrado)
    pygame.display.update()
pygame.quit()

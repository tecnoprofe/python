import pygame
import random
import os

# Inicializa Pygame 
pygame.init()

# Pantalla
ANCHO = 800
ALTO = 600
VENTANA = pygame.display.set_mode((ANCHO, ALTO))

# Título e imagenes
pygame.display.set_caption("TIRO AL BLANCO")
icono = pygame.image.load(os.path.dirname(__file__)+"\\icon.png")
pygame.display.set_icon(icono) 
fondo = pygame.image.load(os.path.dirname(__file__)+"\\wall.png")
comida = pygame.image.load(os.path.dirname(__file__)+"\\icon.png")


# Opciones de dificultad
DIFICULTAD = {"Facil": 0.10, "Normal": 0.15, "Dificil": 0.20}

def menu():
    pygame.font.init()
    fuente = pygame.font.SysFont("Impact", 30)

    dificultad = "Normal"
    
    while True:
        
        VENTANA.blit(fondo, (0,0))

        seleccionar_texto = fuente.render("Selecciona la dificultad", True, (255,255,255))
        VENTANA.blit(seleccionar_texto, (ANCHO/2 - seleccionar_texto.get_width()/2, 150))

        y = 200
        for enter in DIFICULTAD:
            dificultad_texto = fuente.render(enter, 1, (255,255,255))

            if dificultad == enter:
                dificultad_texto = fuente.render(enter, 1, (255,0,0))

            VENTANA.blit(dificultad_texto, (ANCHO/2 - dificultad_texto.get_width()/2, y))
            y += 40

        pygame.display.update()

        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                pygame.quit()
                
            if evento.type == pygame.KEYDOWN:
                if evento.key == pygame.K_UP and dificultad != list(DIFICULTAD.keys())[0]:
                    dificultad = list(DIFICULTAD.keys())[list(DIFICULTAD.keys()).index(dificultad)-1]
                elif evento.key == pygame.K_DOWN and dificultad != list(DIFICULTAD.keys())[-1]:
                    dificultad = list(DIFICULTAD.keys())[list(DIFICULTAD.keys()).index(dificultad)+1]
                elif evento.key == pygame.K_RETURN:
                    return DIFICULTAD[dificultad]
                    
dificultad = menu()

# Cuadrados
squares = []
square_size = 50
comida = pygame.transform.scale(comida, (square_size, square_size))
for i in range(10):
    x = random.randint(25, ANCHO-25)
    y = random.randint(25, 200)
    dx = random.choice([1, -1])
    dy = 1 
    squares.append([x, y, dx, dy, (0,255,0)])  

# Puntaje
score = 0  
font = pygame.font.SysFont('Impact', 32)

def show_score():
    score_text = font.render("Puntaje: " + str(score), True, (255,255,255))  
    VENTANA.blit(score_text, (10, 10))

# Bucle principal
running = True 
while running:

    # Gestion de eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False    

        if event.type == pygame.MOUSEBUTTONDOWN:
            x, y = pygame.mouse.get_pos()
            for i, s in enumerate(squares):
                if x > s[0] and x < s[0] + square_size and y > s[1] and y < s[1] + square_size:
                    del squares[i]
                    score += 1

    # Lógica del juego
    for square in squares:
        square[0] += float(square[2] * dificultad)  
        square[1] += float(square[3] * dificultad)
        if square[0] < 0 or square[0] > ANCHO-square_size:
            square[2] *= -1
        if square[1] < 25 or square[1] > ALTO-square_size:
            square[3] *= -1
            
    # Pintar elementos
    VENTANA.blit(fondo, (0,0)) 

    for square in squares:
        #pygame.draw.rect(VENTANA, square[-1], (square[0], square[1], square_size, square_size))
        VENTANA.blit(comida, (square[0], square[1]))

    show_score()
    pygame.display.update() 
    
pygame.quit()
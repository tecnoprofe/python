import pygame,sys
from pygame.locals import * 

pygame.init()
FPS = 24 #Fotogramas
fpsClock = pygame.time.Clock()

#Ventana
screen = pygame.display.set_mode((1000,800), 0, 32) 
pygame.display.set_caption('Tiempo')

#Colores
black = ( 0, 0, 0) 
white = (255, 255, 255) 
red = (255, 0, 0) 
green = ( 0, 255, 0) 
blue = ( 0, 0, 255)

imageImg = pygame.image.load('C:/Users/DrZam/Pictures/fondos_zoom/Amueblados-de-Sala-Venecia-Muebles-Primiun-.jpg') 

imagex = 320 
imagey = 220

direction = 'left'


# the main game loop 
bandera=True
while True:
    screen.fill(green) 
    
    if direction == 'right': 
        imagex += 5
        if imagex == 320: 
            direction = 'down'
    elif direction == 'down': 
        imagey += 5
        if imagey == 220: 
            direction = 'left'
    elif direction == 'left' and bandera==True: 
        imagex -= 10        
        if imagex == 20: 
            direction = 'up'
    elif direction == 'up': 
        imagey -= 5
        if imagey == 20: 
            direction = 'right' 
    
    screen.blit(imageImg, (imagex, imagey))
    

    for event in pygame.event.get(): 
        if event.type == QUIT: 
            pygame.quit()             
    pygame.display.update() 
    fpsClock.tick(FPS)
#
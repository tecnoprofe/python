import pygame
pygame.init()

#configuracion de pantalla
ventana= pygame.display.set_mode((800,600))
pygame.display.set_caption("Mi primer juego")

# colores
BLANCO=(255,255,255)
NEGRO=(0,0,0)
ROJO=(255,0,0)
AMARILLO=(255,255,0)

# Variables
x=700
y=500
#_____________________________

# Bucle principal
interruptor=True
while interruptor==True:
    # Manejo de eventos
    for evento in pygame.event.get():
        if evento.type== pygame.QUIT:
            interruptor=False
        if evento.type== pygame.KEYDOWN:
            if evento.key == pygame.K_ESCAPE:
                interruptor=False

    #Obtener teclas
    teclas= pygame.key.get_pressed()
    if teclas[pygame.K_LEFT]:
        x=x-1
    if teclas[pygame.K_RIGHT]:
        x=x+1
    if teclas[pygame.K_UP]:
        y=y-1
    if teclas[pygame.K_DOWN]:
        y=y+1

    # Creacion de rectangulos
    barra= pygame.Rect(x,y,100,10)
    pared_izquierda=pygame.Rect(0,0,50,600)
    pared_arriba= pygame.Rect(0,0,800,50)
    
    # COlisiones
    if barra.colliderect(pared_izquierda):
        x=50
    if barra.colliderect(pared_arriba):
        y=50

    #dibujar la pantalla
    ventana.fill(AMARILLO)
    pygame.draw.rect(ventana,NEGRO,pared_izquierda)
    pygame.draw.rect(ventana,NEGRO,pared_arriba)
    pygame.draw.rect(ventana,NEGRO,(750,0,50,600))
    pygame.draw.rect(ventana,NEGRO,(0,550,800,50))
    pygame.draw.rect(ventana,ROJO,barra)

    




    # Actualizar pantalla
    pygame.display.flip()
#_____________________________
# fin del programa
pygame.quit()
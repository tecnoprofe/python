import pygame
import sys

# Inicializar pygame
pygame.init()

# Configuración de la pantalla
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Jugador que Dispara Proyectiles")

# Colores
WHITE = (255, 255, 255)
BLUE = (0, 128, 255)
RED = (255, 0, 0)

# Reloj para controlar los FPS
clock = pygame.time.Clock()
fps = 60

# Posición y tamaño del jugador
player_x = screen_width // 2
player_y = screen_height // 2
player_size = 50
player_speed = 5

# Lista de proyectiles
projectiles = []
projectile_speed = 7

# Bucle principal del juego
running = True
while running:
    # Manejo de eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
            if event.key == pygame.K_SPACE:
                projectiles.append([player_x + player_size // 2, player_y])

    # Obtener el estado actual de todas las teclas
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player_x -= player_speed
    if keys[pygame.K_RIGHT]:
        player_x += player_speed
    if keys[pygame.K_UP]:
        player_y -= player_speed
    if keys[pygame.K_DOWN]:
        player_y += player_speed

    # Limitar el movimiento del jugador dentro de la ventana
    if player_x < 0:
        player_x = 0
    if player_x > screen_width - player_size:
        player_x = screen_width - player_size
    if player_y < 0:
        player_y = 0
    if player_y > screen_height - player_size:
        player_y = screen_height - player_size

    # Mover los proyectiles
    for projectile in projectiles:
        projectile[1] -= projectile_speed
        if projectile[1] < 0:
            projectiles.remove(projectile)

    # Dibujar en pantalla
    screen.fill(WHITE)  # Pintar el fondo de blanco
    pygame.draw.rect(screen, BLUE, (player_x, player_y, player_size, player_size))
    for projectile in projectiles:
        pygame.draw.rect(screen, RED, (projectile[0], projectile[1], 5, 10))

    # Actualizar la pantalla
    pygame.display.flip()

    # Controlar los FPS
    clock.tick(fps)

# Salir de pygame
pygame.quit()
sys.exit()

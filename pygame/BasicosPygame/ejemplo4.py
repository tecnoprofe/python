import pygame
import sys
import random

# Inicializar pygame
pygame.init()

# Configuración de la pantalla
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Pelota que Cambia de Color")

# Colores
WHITE = (255, 255, 255)
colors = [(0, 128, 255), (255, 0, 0), (0, 255, 0), (255, 255, 0)]

# Reloj para controlar los FPS
clock = pygame.time.Clock()
fps = 60

# Posición y tamaño de la pelota
ball_x = screen_width // 2
ball_y = screen_height // 2
ball_radius = 20
ball_speed_x = 5
ball_speed_y = 5
ball_color = random.choice(colors)

# Bucle principal del juego
running = True
while running:
    # Manejo de eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False

    # Mover la pelota
    ball_x += ball_speed_x
    ball_y += ball_speed_y

    # Rebotar en las paredes y cambiar de color
    if ball_x - ball_radius < 0 or ball_x + ball_radius > screen_width:
        ball_speed_x = -ball_speed_x
        ball_color = random.choice(colors)
    if ball_y - ball_radius < 0 or ball_y + ball_radius > screen_height:
        ball_speed_y = -ball_speed_y
        ball_color = random.choice(colors)

    # Dibujar en pantalla
    screen.fill(WHITE)  # Pintar el fondo de blanco
    pygame.draw.circle(screen, ball_color, (ball_x, ball_y), ball_radius)

    # Actualizar la pantalla
    pygame.display.flip()

    # Controlar los FPS
    clock.tick(fps)

# Salir de pygame
pygame.quit()
sys.exit()

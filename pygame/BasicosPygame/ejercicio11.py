import pygame
import sys
import random

# Inicializar pygame
pygame.init()

# Configuración de la pantalla
ventana_ancho = 800
ventana_alto = 600
ventana = pygame.display.set_mode((ventana_ancho, ventana_alto))
pygame.display.set_caption("proyecto Grupo 1")



# colores
BLANCO=(255,255,255)
NEGRO=(0,0,0)
COLOR_ALEATORIO=(random.randint(0,255),random.randint(0,255),random.randint(0,255))

cuadrado_ancho=50
cuadrado_alto=50
cuadrado_x=100
cuadrado_y=100

contador=1
# Reloj para controlar los FPS
clock = pygame.time.Clock()
fps =150
points=0
running = True
while running:
     # Manejo de eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False

    cuadrado= pygame.Rect(cuadrado_x,cuadrado_y,cuadrado_ancho,cuadrado_alto)    
    cuadrado_x+=contador
    

    if cuadrado_x >800:
        contador=contador*-1
        fps=fps+10
        points=points+1
    if cuadrado_x <0:
        contador=contador*-1
        fps=fps+10
        points=points+1


    ventana.fill(BLANCO)
    pygame.draw.rect(ventana,COLOR_ALEATORIO,cuadrado)
    pygame.draw.circle(ventana, COLOR_ALEATORIO,(200,200),10)

    
    # Mostrar los puntos
    font = pygame.font.SysFont(None, 55)
    points_text = font.render(f'Puntos: {points}', True, NEGRO)
    ventana.blit(points_text, (10, 10))

    if points>3:
        font = pygame.font.SysFont(None, 160)
        texto_ganador = font.render("Ganador", True, NEGRO)
        ventana.blit(texto_ganador, (200, 300))


    # Actualizar la pantalla
    pygame.display.flip()
     # Controlar los FPS
    clock.tick(fps)
# Salir de pygame
pygame.quit()
sys.exit()

    
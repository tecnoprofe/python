import pygame
import sys
import random

# Inicializar pygame
pygame.init()

# Configuración de la pantalla
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Evitar Obstáculos")

# Colores
WHITE = (255, 255, 255)
BLUE = (0, 128, 255)
RED = (255, 0, 0)

# Reloj para controlar los FPS
clock = pygame.time.Clock()
fps = 60

# Posición y tamaño del jugador
player_x = screen_width // 2
player_y = screen_height // 2
player_size = 50
player_speed = 5

# Posición y tamaño del obstáculo
obstacle_x = random.randint(0, screen_width - player_size)
obstacle_y = random.randint(0, screen_height - player_size)
obstacle_size = 50

# Bucle principal del juego
running = True
while running:
    # Manejo de eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False

    # Obtener el estado actual de todas las teclas
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player_x -= player_speed
    if keys[pygame.K_RIGHT]:
        player_x += player_speed
    if keys[pygame.K_UP]:
        player_y -= player_speed
    if keys[pygame.K_DOWN]:
        player_y += player_speed

    # Limitar el movimiento del jugador dentro de la ventana
    if player_x < 0:
        player_x = 0
    if player_x > screen_width - player_size:
        player_x = screen_width - player_size
    if player_y < 0:
        player_y = 0
    if player_y > screen_height - player_size:
        player_y = screen_height - player_size

    # Crear rectángulos para la colisión
    player_rect = pygame.Rect(player_x, player_y, player_size, player_size)
    obstacle_rect = pygame.Rect(obstacle_x, obstacle_y, obstacle_size, obstacle_size)

    # Detectar colisión y reiniciar posición del jugador
    if player_rect.colliderect(obstacle_rect):
        player_x = screen_width // 2
        player_y = screen_height // 2

    # Dibujar en pantalla
    screen.fill(WHITE)  # Pintar el fondo de blanco
    pygame.draw.rect(screen, BLUE, player_rect)
    pygame.draw.rect(screen, RED, obstacle_rect)

    # Actualizar la pantalla
    pygame.display.flip()

    # Controlar los FPS
    clock.tick(fps)

# Salir de pygame
pygame.quit()
sys.exit()

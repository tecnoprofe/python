import pygame
import sys
import random

# Inicializar pygame
pygame.init()

# Configuración de la pantalla
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("Recoger Monedas")

# Colores
WHITE = (255, 255, 255)
BLUE = (0, 128, 255)
GOLD = (255, 215, 0)

# Reloj para controlar los FPS
clock = pygame.time.Clock()
fps = 60

# Posición y tamaño del jugador
player_x = screen_width // 2
player_y = screen_height // 2
player_size = 50
player_speed = 5

# Posición y tamaño de las monedas
coins = []
for _ in range(5):
    coin_x = random.randint(0, screen_width - player_size)
    coin_y = random.randint(0, screen_height - player_size)
    coins.append([coin_x, coin_y, 20])

# Puntos del jugador
points = 0

# Bucle principal del juego
running = True
while running:
    # Manejo de eventos
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False

    # Obtener el estado actual de todas las teclas
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT]:
        player_x -= player_speed
    if keys[pygame.K_RIGHT]:
        player_x += player_speed
    if keys[pygame.K_UP]:
        player_y -= player_speed
    if keys[pygame.K_DOWN]:
        player_y += player_speed

    # Limitar el movimiento del jugador dentro de la ventana
    if player_x < 0:
        player_x = 0
    if player_x > screen_width - player_size:
        player_x = screen_width - player_size
    if player_y < 0:
        player_y = 0
    if player_y > screen_height - player_size:
        player_y = screen_height - player_size

    # Crear rectángulos para la colisión
    player_rect = pygame.Rect(player_x, player_y, player_size, player_size)

    # Detectar colisiones con las monedas
    for coin in coins[:]:
        coin_rect = pygame.Rect(coin[0], coin[1], coin[2], coin[2])
        if player_rect.colliderect(coin_rect):
            points += 1
            coins.remove(coin)

    # Dibujar en pantalla
    screen.fill(WHITE)  # Pintar el fondo de blanco
    pygame.draw.rect(screen, BLUE, player_rect)
    for coin in coins:
        pygame.draw.circle(screen, GOLD, (coin[0] + coin[2] // 2, coin[1] + coin[2] // 2), coin[2] // 2)

    # Mostrar los puntos
    font = pygame.font.SysFont(None, 55)
    points_text = font.render(f'Puntos: {points}', True, BLUE)
    screen.blit(points_text, (10, 10))

    # Actualizar la pantalla
    pygame.display.flip()

    # Controlar los FPS
    clock.tick(fps)

# Salir de pygame
pygame.quit()
sys.exit()

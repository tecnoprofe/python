class Animal:
    nombre=None     
    genero=None
    edad=None   
    color=None
    peso=None
    tipo=None
    _codigo=None

    def come(self):
        print(self.nombre," come")
    
    def duerme(self):
        print(self.nombre," duerme")

    def caza(self):
        print(self.nombre," caza")
        
class Leon(Animal):
    cabellera=None
    def __init__(self,nombre,genero,edad,color,tipo=None,peso=None,codigo=None):
        self.nombre=nombre
        self.genero=genero
        self.edad=edad
        self.color=color
        self.tipo=tipo
        self.peso=peso
        self._codigo=codigo

    def ruge(self):
        print(self.nombre," dice grrrrraaawwww")


leonel=Leon("Leonel","Macho",10,"Naranja")
print("--------Leon ", leonel.nombre)
leonel._codigo="#45456"
print("Mi codigo essss",leonel._codigo)
leonel.ruge()
leonel.come()
leonel.duerme()


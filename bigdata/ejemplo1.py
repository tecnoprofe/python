a=3



b=8
c=a+b
print(c)
if (c>11):
    for i in range(c): 
        print(" C es mayor")
        if (c>11):
            for i in range(c): 
                print(" C es mayor")
    print(" Entendido?")
    print(" Chau")


#S01
# Se muestra un mensaje en pantalla pidiendo que se ingrese un nombre
print("Ingrese su nombre")

# Se guarda el nombre ingresado por el usuario en la variable 'nombre'
nombre = input()

# Se muestra un mensaje en pantalla dándole la bienvenida al usuario con el nombre que ingresó
print("Ahora ya eres parte de nuestro sistema: " + nombre)




#S02
# Se imprime en pantalla el mensaje "INGRESE LA PRIMERA PALABRA"
print("INGRESE LA PRIMERA PALABRA")

# Se asigna el valor ingresado por el usuario a la variable "cadena1"
cadena1 = input()

# Se imprime en pantalla el mensaje "INGRESE LA SEGUNDA PALABRA"
print("INGRESE LA SEGUNDA PALABRA")

# Se asigna el valor ingresado por el usuario a la variable "cadena2"
cadena2 = input()

# Se concatena la variable "cadena1" y "cadena2" y se asigna el resultado a la variable "resultado"
resultado = cadena1 + " " + cadena2

# Se imprime en pantalla el resultado de la concatenación de las variables "cadena1" y "cadena2"
print(resultado)


#S03
# Pide al usuario que introduzca la cantidad de manzanas
print("Introduce cantidad de manzanas")

# Almacena la cantidad de manzanas introducidas por el usuario en la variable 'manzanas'
manzanas = int(input())

# Pide al usuario que introduzca la cantidad de peras
print("Introduce cantidad de peras")

# Almacena la cantidad de peras introducidas por el usuario en la variable 'peras'
peras = int(input())

# Suma la cantidad de manzanas y peras y almacena el resultado en la variable 'frutero'
frutero = manzanas + peras

# Imprime el resultado de la suma de manzanas y peras en la pantalla
print("Total Frutas: " + str(frutero))






#S04
# Se imprime un mensaje solicitando al usuario que ingrese la temperatura en grados Celsius
print("Ingrese una temperatura en Grados Celsius: ")
# Se almacena el valor ingresado por el usuario en una variable de tipo flotante llamada "celsius"
celsius = float(input())
# Se realiza la conversión de grados Celsius a Fahrenheit y se almacena el resultado en una variable llamada "fahrenheit"
fahrenheit = 1.8 * celsius + 32
# Se imprime el resultado de la conversión en grados Fahrenheit en forma de cadena utilizando la función "str()"
print("La temperatura es en Grados Fahrenheit es: " + str(fahrenheit))



#S05
# Se solicita al usuario que ingrese la cantidad de metros a convertir
print("Ingrese la cantidad de metros: ")

# Se asigna el valor ingresado a la variable "metros" y se convierte a un número decimal
metros = float(input())

# Se realiza la conversión a centímetros y se asigna el resultado a la variable "centimetros"
centimetros = metros * 100

# Se realiza la conversión a milímetros y se asigna el resultado a la variable "milimetros"
milimetros = metros * 1000

# Se muestra el resultado de la conversión a centímetros mediante la concatenación de strings
print(str(metros) + " metros son " + str(centimetros) + " centímetros")

# Se muestra el resultado de la conversión a milímetros mediante la concatenación de strings
print(str(metros) + " metros son " + str(milimetros) + " milímetros")



#S06
# Se imprime en pantalla el mensaje para que el usuario ingrese el precio del producto
print("Ingrese precio del producto")

# Se guarda en la variable 'producto' el valor ingresado por el usuario, convertido a tipo float
producto = float(input())

# Se calcula el valor del IVA multiplicando el valor del producto por 0.13
iVA = producto * 0.13

# Se calcula el precio total a pagar, sumando el valor del producto y el valor del IVA
precioConIVA = iVA + producto

# Se imprime en pantalla el precio total a pagar más el IVA, con un mensaje de texto y utilizando la función str() para convertir el valor a tipo string
print("El precio total a pagar más IVA es : " + str(precioConIVA) + " BS.")

#S07
# Se pide al usuario que ingrese la cantidad de kilómetros recorridos por la motocicleta
print("Ingrese los kilometros recorridos en km ")
kilometro = float(input())

# Se pide al usuario que ingrese la cantidad de litros de combustible consumidos
print("Ingrese la cantidad de combustible consumidos en el recorrido en litros")
combustible = float(input())

# Se realiza el cálculo del consumo de combustible por kilómetro
resultado = combustible / kilometro

# Se muestra el resultado al usuario
print("La cantidad de combustible es : " + str(resultado) + " LTS." + " por cada kilometro recorrido")




#S08
# Mensaje para solicitar al usuario que ingrese un valor
print("Ingrese un valor ")

# Se utiliza la función "input()" para obtener el valor ingresado por el usuario, y se convierte a un número decimal utilizando "float()"
precio = float(input())

# Se calcula el precio con el descuento del 15% y se guarda en la variable "resultado"
resultado = precio - precio * 0.15

# Se muestra el precio original ingresado por el usuario en pantalla
print("Precio real: " + str(precio))

# Se muestra el precio con el descuento del 15% en pantalla
print("Precio aplicando el 15% de descuento: " + str(resultado))


#S09
# Se muestra el mensaje "Ingrese el primer numero" para solicitar al usuario que ingrese el primer número
print("Ingrese el primer numero")

# Se almacena el primer número ingresado por el usuario en la variable n1, usando la función input() para solicitar la entrada de datos, y la función float() para convertir el valor ingresado en un número decimal (punto flotante)
n1 = float(input())

# Se muestra el mensaje "Ingrese el segundo numero" para solicitar al usuario que ingrese el segundo número
print("Ingrese el segundo numero")

# Se almacena el segundo número ingresado por el usuario en la variable n2, usando la función input() para solicitar la entrada de datos, y la función float() para convertir el valor ingresado en un número decimal (punto flotante)
n2 = float(input())

# Se muestra el mensaje "Ingrese el tercer numero" para solicitar al usuario que ingrese el tercer número
print("Ingrese el tercer numero")

# Se almacena el tercer número ingresado por el usuario en la variable n3, usando la función input() para solicitar la entrada de datos, y la función float() para convertir el valor ingresado en un número decimal (punto flotante)
n3 = float(input())

# Se calcula el promedio de los tres números ingresados, sumando los tres números y dividiendo el resultado entre 3. El resultado se almacena en la variable resultado.
resultado = (n1 + n2 + n3) / 3

# Se muestra el mensaje "El promedio de los tres numeros ingresados es: " seguido del valor del resultado, que se convierte a una cadena de texto (string) usando la función str().
print("El promedio de los tres numeros ingresados es : " + str(resultado))





# S10
# Imprime un mensaje pidiendo al usuario que ingrese la fecha en el formato DDMMAAAA
print("Ingrese la fecha DDMMAAAA")

# Lee la entrada del usuario y la almacena como un entero en la variable 'fecha'
fecha = int(input())

# Extrae el día dividiendo 'fecha' por 1,000,000 y convirtiendo el resultado en un entero
dia = int (fecha / 1000000)

# Extrae el mes dividiendo 'fecha' por 10,000, tomando el módulo 100 del resultado y convirtiendo el resultado en un entero
mes = int (fecha / 10000) % 100

# Extrae el año tomando el módulo 10,000 de 'fecha'
gestion = fecha % 10000

# Imprime la fecha ingresada en el formato "D/M/AAAA", concatenando las variables 'dia', 'mes' y 'gestion' como cadenas
print("La fecha ingresada es : " + str(dia) + "/" + str(mes) + "/" + str(gestion))


# S11
# Importa el módulo 'math' para acceder a funciones y constantes matemáticas, como 'math.pi'
import math

# Solicita al usuario ingresar el radio del círculo y lo almacena como un número de tipo 'float' en la variable 'radio'
radio = float(input("Introduce el radio del círculo: "))

# Calcula el área del círculo utilizando la fórmula: área = π * r^2
# Donde 'math.pi' representa el valor de π y 'radio ** 2' eleva el radio al cuadrado
area = math.pi * radio ** 2

# Imprime el área del círculo en la salida, mostrando el resultado almacenado en la variable 'area'
print("El área del círculo es:", area)



# S12. Area de un triangulo rectangulo.
# Pide la base del triángulo rectángulo
print("Introduce la base")
base = float(input())

# Pide la altura del triángulo rectángulo
print("Introduce la altura")
altura = float(input())

# Calcula el área del triángulo rectángulo
area = base * altura / 2

# Imprime el área del triángulo rectángulo
print("El área del triangulo es: " + str(area))




# S13. Área y perímetro de un rectangulo.
# Pide la base del rectángulo
print("Ingrese la Base del rectangulo: ")
base = float(input())

# Pide la altura del rectángulo
print("Ingrese la Altura del rectangulo: ")
altura = float(input())

# Calcula el área del rectángulo
area = base * altura

# Calcula el perímetro del rectángulo
perimetro = 2 * (base + altura)

# Imprime el área y perímetro del rectángulo
print("El área del rectangulo es: " + str(area) + " y el perimetro es: " + str(perimetro))


#capitulo 2
#Ejemplo 1
# Asigna el valor 18 a la variable 'edad'
edad = 18

# Verifica si el valor de 'edad' es mayor o igual a 18
if edad >= 18:
    # Imprime "Mayor de edad" si la condición es verdadera
    print("Mayor de edad")


#ejemplo 2
# Asigna 18 a 'edad'
edad = 18

# Comprueba si 'edad' es mayor o igual a 18
if edad >= 18:
    # Imprime "Mayor de edad"
    print("Mayor de edad")
else:
    # Imprime "Menor de edad"
    print("Menor de edad")



edad = 25
ciudadano = True

if edad >= 18 and ciudadano:
    print("Eres elegible para votar.")
else:
    print("No eres elegible para votar.")

#or
llueve = False
tengo_paraguas = True

if llueve or tengo_paraguas:
    print("Puedo salir sin mojarme.")
else:
    print("Me voy a mojar si salgo.")


#NOT
puerta_abierta = False

if not puerta_abierta:
    print("La puerta está cerrada.")
else:
    print("La puerta está abierta.")


# C01
# Imprime en pantalla la pregunta para que el usuario ingrese la cantidad de conciertos vistos este año
print("¿Ingrese la cantidad de conciertos musicales que usted a visto este año?")

# Toma el valor ingresado por el usuario, lo convierte a un entero y lo almacena en la variable 'concierto'
concierto = int(input())

# Comprueba si la cantidad de conciertos almacenada en 'concierto' es mayor a 3
if concierto > 3:
    # Imprime en pantalla que el usuario ha visto más de 3 conciertos si la condición se cumple
    print("Usted ha visto mas de 3 conciertos")


    
# C02
# Se pide la edad 
print("Ingrese su edad: ")
edad = int(input())

# Se pide la cantidad de artículos comprados
print("Ingrese, la cantidad de artículos comprados")
articulo = int(input())

# Comprueba si la edad es mayor o igual a 18 y si la cantidad de artículos comprados es mayor a 1
if edad >= 18 and articulo > 1:    
    print("Usted es mayor de edad y compro " + str(articulo) + " artículos")
else:    
    print("Compra negada")
    

# C03
# Se pide que ingrese su nombre
print("Ingrese su nombre")
nombre = input()

# Se pide al usuario que ingrese su edad
print("Ingrese su edad")
edad = int(input())

# Comprueba si la edad es mayor o igual a 18
if edad >= 18:    
    print("Usted " + nombre + " puede votar")
else:    
    print("Usted " + nombre + " NO puede votar")

# C04
# Pide al usuario que ingrese un número
print("Introduzca un número")
n = int(input())
# Calcula el residuo de la división de 'n' entre 2 y lo almacena en la variable 'resultado'
resultado = n % 2
# Comprueba si el residuo es igual a 0
if resultado == 0:    
    print("Número Par")
else:    
    print("Número Impar")





texto = "Hola, mundo!"
longitud = len(texto)
print(longitud)  # Resultado: 12


# C05

# Pide al usuario ingrese un texto
print("Ingrese un texto: ")
texto = input()
# Calcula la longitud del texto y la almacena en la variable 'longitud'
longitud = len(texto)
# Comprueba si la longitud del texto es par
if longitud % 2 == 0:    
    print("El Texto es Par, tiene " + str(longitud) + " caracteres")
else:
    print("El Texto es Impar, tiene " + str(longitud) + " caracteres")

# C06
# Pide al usuario usuario ingrese un número
print("Ingrese un numero")
numero = int(input())
# Comprueba si el residuo de la división del número entre 5 es igual a 0
if numero % 5 == 0:    
    print(str(numero) + " Es multiplo de 5")
else:
    print(str(numero) + " No es Multiplo de 5")




numero = 10
cadena = str(numero)
print(cadena)

cadena = "10"
numero = int(cadena)
print(numero)



# Números enteros
a = 5
b = -2


sumas = 0
multiplicaciones = 1
print("Introduzca un límite para esta seríe")
n = int(input())
for i in range(1, n + 1, 1):
    print("Introduzca un número")
    valor = int(input())
    if valor % 2 == 0:
        sumas = sumas + valor
    else:
        multiplicaciones = multiplicaciones * valor
print("La suma acumulada de numeros pares, es: " + str(sumas))
print("EL producto acumulado de numeros impares, es: " + str(multiplicaciones))


print("Introduzca un número Positivo: ")
n = int(input())
a = 0
b = 1
for i in range(1, n + 1, 1):
    auxiliar = a + b
    print(auxiliar)
    a = b
    b = auxiliar



# Definición de la función celsiusAfahrenheit
def celsiusAfahrenheit(celsius):
    fahrenheit = celsius * 9 / 5 + 32  # Aplica la fórmula de conversión de Celsius a Fahrenheit
    
    return fahrenheit

# Definición de la función fahrenheitACelsius
def fahrenheitACelsius(fahrenheit):
    celsius = (fahrenheit - 32) * 5 / 9  # Aplica la fórmula de conversión de Fahrenheit a Celsius
    
    return celsius

# Bloque principal (Main)
celsius = 25  # Temperatura en grados Celsius
fahrenheit = 77  # Temperatura en grados Fahrenheit

# Convierte Celsius a Fahrenheit y muestra el resultado
print(str(celsius) + " grados en Celsius es: " + str(celsiusAfahrenheit(celsius)) + " grados Fahrenheit")

# Convierte Fahrenheit a Celsius y muestra el resultado
print(str(fahrenheit) + " grados en Fahrenheit es: " + str(fahrenheitACelsius(fahrenheit)) + " grados Celsius")




edad = 18

if edad < 13:
    print("Eres un niño.")
elif edad < 18:
    print("Eres un adolescente.")
elif edad < 65:
    print("Eres un adulto.")
else:
    print("Eres un adulto mayor.")



# Asignamos el valor 18 a la variable edad
edad = 18 

# Comprobamos si la edad es menor que 13
if edad < 13:
    # Si es verdad, se imprime "Eres un niño."
    print("Eres un niño.") 
# Si la condición anterior es falsa, comprobamos si la edad es menor que 18
elif edad < 18:
    # Si es verdad, se imprime "Eres un adolescente."
    print("Eres un adolescente.")
# Si las condiciones anteriores son falsas, comprobamos si la edad es menor que 65
elif edad < 65:
    # Si es verdad, se imprime "Eres un adulto."
    print("Eres un adulto.")
# Si todas las condiciones anteriores son falsas, se ejecuta el código en el bloque 'else'
else:
    # Se imprime "Eres un adulto mayor."
    print("Eres un adulto mayor.")

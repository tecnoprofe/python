import turtle

for i in range(10):
    radius = 10 + i*20
    turtle.circle(radius)
    turtle.up()
    turtle.circle(radius + 10)
    turtle.down()

turtle.done()

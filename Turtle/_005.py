import turtle

# Crea una nueva tortuga
t = turtle.Turtle()
t.speed(1)

for i in range(7):
    t.forward(100)
    t.left(90)
    t.forward(100)
    t.right(90)
    t.forward(50)
    t.right(90)
    t.forward(150)
    t.right(90)
    t.forward(150)
    t.right(90)
    t.forward(50)

# Mantén la ventana abierta hasta que se cierre manualmente
turtle.done()

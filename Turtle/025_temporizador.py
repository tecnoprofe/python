import turtle
import time

# Configuración de la ventana
wn = turtle.Screen()
wn.title("Temporizador Regresivo con Turtle")
wn.bgcolor("black")

# Configuración de la tortuga para mostrar el tiempo
contador = turtle.Turtle()
contador.color("white")
contador.penup()
contador.hideturtle()
contador.goto(0, 0)

# Tiempo inicial del temporizador (en segundos)
tiempo_inicial = 300  # 1 minuto

# Función para actualizar el tiempo
def actualizar_temporizador(segundos_restantes):
    contador.clear()
    contador.write(f"{segundos_restantes} segundos", align="center", font=("Courier", 24, "normal"))
    if segundos_restantes > 0:
        # Llamar a esta función nuevamente después de 1 segundo con el tiempo reducido
        wn.ontimer(lambda: actualizar_temporizador(segundos_restantes - 1), 1000)
    else:
        contador.write("¡Tiempo Agotado!", align="center", font=("Courier", 24, "normal"))

# Iniciar el temporizador
actualizar_temporizador(tiempo_inicial)

# Mantener abierta la ventana
wn.mainloop()

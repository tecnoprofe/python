import turtle

def draw_star(size):
    for _ in range(5):
        turtle.forward(size)
        turtle.right(144)

for _ in range(12):
    draw_star(50)
    turtle.right(30)

turtle.done()

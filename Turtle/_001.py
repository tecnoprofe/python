import turtle

t = turtle.Turtle()

# Establece el color de la pluma (borde) y el color de relleno
t.color("green", "red")

# Dibuja el primer círculo con relleno
t.begin_fill()
t.circle(40)
t.end_fill()

# Levanta la pluma
t.penup()

# Desplaza la tortuga a otra ubicación
t.goto(200, 0)

# Baja la pluma
t.pendown()

# Dibuja el segundo círculo con relleno
t.begin_fill()
t.circle(190)
t.end_fill()

turtle.done()

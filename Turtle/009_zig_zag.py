import turtle

for _ in range(5):
    turtle.left(45)
    turtle.forward(50)
    turtle.right(90)
    turtle.forward(50)
    turtle.left(45)

turtle.done()

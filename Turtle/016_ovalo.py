import turtle

turtle.up()
turtle.goto(-50, 0)
turtle.down()

for _ in range(2):
    turtle.circle(50, 180)
    turtle.forward(100)

turtle.done()

import turtle

for sides in range(3, 9):
    for _ in range(sides):
        turtle.forward(60)
        turtle.left(360 / sides)
    turtle.right(360 / (sides * 2))

turtle.done()

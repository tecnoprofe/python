import turtle

# Dibuja la parte izquierda del corazón
turtle.left(50)
turtle.forward(133)
turtle.circle(50, 200)

# Posición para la parte derecha del corazón
turtle.right(140)
turtle.circle(50, 200)
turtle.forward(133)

turtle.done()
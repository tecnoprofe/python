import turtle

def draw_tree(t, branch_length):
    if branch_length > 5:
        # Dibujar el tronco (rama principal)
        t.forward(branch_length)

        # Dibujar la rama derecha
        t.right(20)
        draw_tree(t, branch_length - 15)

        # Regresar al camino principal y dibujar la rama izquierda
        t.left(40)
        draw_tree(t, branch_length - 15)

        # Regresar al camino principal y ajustar la orientación
        t.right(20)
        t.backward(branch_length)

# Configuraciones iniciales
window = turtle.Screen()
window.bgcolor("white")

t = turtle.Turtle()
t.speed(0)  # La velocidad más rápida
t.width(3)
t.color("green")
t.left(90)  # Hacer que la tortuga mire hacia arriba

# Iniciar el dibujo desde la parte inferior de la ventana
t.up()
t.backward(150)
t.down()

# Dibujar el árbol fractal
draw_tree(t, 100)

# Finalizar el dibujo
window.mainloop()

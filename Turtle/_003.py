
import turtle

t = turtle.Turtle()
screen = turtle.Screen()
screen.bgcolor("black")  # Establecer el color de fondo de la pantalla
colors = ["red", "orange", "yellow", "green", "blue", "purple"]

t.speed(5)

for x in range(360):
    t.color(colors[x % 6])  # Cambio de color
    t.forward(x)
    t.left(59)

turtle.done()


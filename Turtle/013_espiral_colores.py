import turtle

colors = ['red', 'blue', 'green', 'yellow']
radius = 10

for i in range(36):
    turtle.color(colors[i % 4])
    turtle.circle(radius)
    radius += 5
    turtle.right(10)

turtle.done()

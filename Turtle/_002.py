import turtle

# Configuración inicial de la tortuga
t = turtle.Turtle()
t.speed(1)  # Velocidad de la tortuga (1=lento, 10=rápido)

# Dibujo de un cuadrado
for _ in range(4):
    t.forward(100)  # Mover 100 unidades hacia adelante
    t.left(90)      # Girar 90 grados a la izquierda
turtle.done()
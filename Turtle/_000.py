import turtle
turtle.Turtle()
turtle.pencolor("blue") 

def forma(grado,lados,distancia):
    for i in range(lados):
        turtle.forward(distancia)
        turtle.left(grado)

for _ in range(5):
    forma(25,15,70)
    forma(50,10,100)    

turtle.done()
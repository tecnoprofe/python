# Dibujar cuadrado simple
import turtle
ancho=15
turtle.colormode(255)

for i in range(20):    
    turtle.pencolor((20,255-i*10,30+i*10))
    turtle.forward(100+i*ancho)
    turtle.left(90)
    turtle.pensize(i)

turtle.done()
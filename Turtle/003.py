# Dibujar casa simple
import turtle

# Paredes de la casa
for _ in range(4):
    turtle.forward(100)
    turtle.left(90)

# Techo de la casa
turtle.goto(0, 100)
for _ in range(3):
    turtle.forward(100)
    turtle.left(120)

turtle.done()
import turtle
import random


# Configurar la pantalla
wn = turtle.Screen()
wn.title("Juego de Recoger Objetos con Turtle")
wn.bgcolor("black")
wn.setup(width=600, height=600)

# Crear la tortuga jugador
player = turtle.Turtle()
player.shape("circle")
player.color("white")
player.penup()
player.speed(0)
player.goto(0, -250)

# Crear el objetivo
target = turtle.Turtle()
target.shape("circle")
target.color("gold")
target.penup()
target.speed(0)
target.shapesize(0.5, 0.5)
target.goto(random.randint(-290, 290), random.randint(-290, 290))

# Puntuación
score = 0

# Funciones para mover al jugador
def go_up():
    y = player.ycor()
    if y < 290:
        player.sety(y + 20)

def go_down():
    y = player.ycor()
    if y > -290:
        player.sety(y - 20)

def go_left():
    x = player.xcor()
    if x > -290:
        player.setx(x - 20)

def go_right():
    x = player.xcor()
    if x < 290:
        player.setx(x + 20)

# Vincular las funciones de movimiento a las teclas
wn.listen()
wn.onkeypress(go_up, "Up")
wn.onkeypress(go_down, "Down")
wn.onkeypress(go_left, "Left")
wn.onkeypress(go_right, "Right")

# Función para mover el objetivo
def move_target():
    target.goto(random.randint(-290, 290), random.randint(-290, 290))

# Función para revisar si se recoge el objetivo
def check_collect():
    global score
    if player.distance(target) < 15:
        move_target()
        score += 10
        print(f"Puntuación: {score}")

# Main game loop
def main_game():
    wn.update()
    check_collect()
    wn.ontimer(main_game, 100)  # Set to call main_game every 100ms

# Llamar al juego principal para iniciar el bucle
main_game()

# Mantener abierta la ventana
wn.mainloop()

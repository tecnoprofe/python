import turtle
import time
import random

# Configuración inicial de la pantalla
wn = turtle.Screen()
wn.title('Juego de la Serpiente con Turtle')
wn.bgcolor('black')
wn.setup(width=600, height=600)
wn.tracer(0)  # Desactiva la actualización automática de la pantalla

# Cabeza de la serpiente
head = turtle.Turtle()
head.shape('turtle')
head.color('green')
head.penup()
head.goto(0, 0)
head.direction = 'stop'

# Comida para la serpiente
food = turtle.Turtle()
food.speed(0)
food.shape('circle')
food.color('red')
food.penup()
food.goto(0, 100)

segments = []

# Funciones para mover la serpiente
def go_up():
    if head.direction != 'down':
        head.direction = 'up'

def go_down():
    if head.direction != 'up':
        head.direction = 'down'

def go_left():
    if head.direction != 'right':
        head.direction = 'left'

def go_right():
    if head.direction != 'left':
        head.direction = 'right'

# Función que mueve la cabeza de la serpiente
def move():
    if head.direction == 'up':
        y = head.ycor()
        head.sety(y + 20)

    if head.direction == 'down':
        y = head.ycor()
        head.sety(y - 20)

    if head.direction == 'left':
        x = head.xcor()
        head.setx(x - 20)

    if head.direction == 'right':
        x = head.xcor()
        head.setx(x + 20)

# Eventos del teclado
wn.listen()
wn.onkeypress(go_up, 'Up')
wn.onkeypress(go_down, 'Down')
wn.onkeypress(go_left, 'Left')
wn.onkeypress(go_right, 'Right')

# Función principal del juego
while True:
    wn.update()

    # Chequear colisión con la comida
    if head.distance(food) < 20:
        # Mover la comida a una posición aleatoria
        x = random.randint(-290, 290)
        y = random.randint(-290, 290)
        food.goto(x, y)

        # Añadir un nuevo segmento a la serpiente
        new_segment = turtle.Turtle()
        new_segment.speed(0)
        new_segment.shape('square')
        new_segment.color('green')
        new_segment.penup()
        segments.append(new_segment)

    # Mover el final de la serpiente primero en orden inverso
    for index in range(len(segments) - 1, 0, -1):
        x = segments[index - 1].xcor()
        y = segments[index - 1].ycor()
        segments[index].goto(x, y)

    # Mover el segmento 0 a donde está la cabeza
    if len(segments) > 0:
        x = head.xcor()
        y = head.ycor()
        segments[0].goto(x, y)

    move()

    # Chequear colisión con el borde
    if head.xcor() > 290 or head.xcor() < -290 or head.ycor() > 290 or head.ycor() < -290:
        time.sleep(1)
        head.goto(0, 0)
        head.direction = 'stop'

        # Esconder los segmentos
        for segment in segments:
            segment.goto(1000, 1000)  # los mueve fuera de la pantalla
        segments.clear()

    # Chequear colisión con la propia serpiente
    for segment in segments:
        if segment.distance(head) < 20:
            time.sleep(1)
            head.goto(0, 0)
            head.direction = 'stop'
            for segment in segments:
                segment.goto(1000, 1000)
            segments.clear()

    time.sleep(0.1)

wn.mainloop()

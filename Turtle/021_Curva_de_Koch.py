import turtle

def koch_curve(t, order, size):
    if order == 0:
        t.forward(size)
    else:
        for angle in [60, -120, 60, 0]:
            koch_curve(t, order-1, size/3)
            t.left(angle)

turtle.speed(0)
for _ in range(3):
    koch_curve(turtle, 3, 200)
    turtle.right(120)
turtle.done()

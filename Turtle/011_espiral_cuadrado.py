import turtle

length = 10
for _ in range(20):
    turtle.forward(length)
    turtle.left(90)
    length += 10

turtle.done()

import turtle
import time

# Configuración de la ventana
wn = turtle.Screen()
wn.title("Cronómetro con Turtle")
wn.bgcolor("black")

# Configuración de la tortuga para mostrar el tiempo
contador = turtle.Turtle()
contador.color("white")
contador.penup()
contador.hideturtle()
contador.goto(0, 0)

# Variable para llevar la cuenta del tiempo
segundos = 0

# Función para actualizar el tiempo
def actualizar_tiempo():
    global segundos
    contador.clear()
    segundos += 1
    contador.write(f"Tiempo: {segundos} segundos", align="center", font=("Courier", 24, "normal"))
    wn.ontimer(actualizar_tiempo, 1000)  # Actualizar cada 1000 ms (1 segundo)

# Iniciar el cronómetro
actualizar_tiempo()

# Mantener abierta la ventana
wn.mainloop()

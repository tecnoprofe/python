import cv2 

def reconocimiento_facial_y_ojos_camara():
    # Cargar los clasificadores preentrenados de detección facial y de ojos
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_eye.xml')

    # Iniciar la cámara frontal
    cap = cv2.VideoCapture(0)

    while True:
        # Leer un fotograma de la cámara
        ret, imagen = cap.read()

        # Convertir el fotograma a escala de grises (los clasificadores esperan una imagen en escala de grises)
        gris = cv2.cvtColor(imagen, cv2.COLOR_BGR2GRAY)

        # Detectar rostros en la imagen
        rostros = face_cascade.detectMultiScale(gris, scaleFactor=1.3, minNeighbors=5)

        # Dibujar rectángulos alrededor de los rostros detectados y detectar los ojos dentro de cada rostro
        for (x, y, w, h) in rostros:
            cv2.rectangle(imagen, (x, y), (x+w, y+h), (255, 0, 0), 2)
            roi_gris = gris[y:y+h, x:x+w]
            roi_color = imagen[y:y+h, x:x+w]
            
            
             # Detectar ojos dentro de cada rostro
            ojos = eye_cascade.detectMultiScale(roi_gris)
            for (ex, ey, ew, eh) in ojos:
                cv2.rectangle(roi_color, (ex, ey), (ex+eh ,ey+ew), (0, 255, 0), 2)
                

        # Mostrar la imagen con los rostros y ojos detectados
        cv2.imshow('Reconocimiento Facial y Ojos', imagen)

        # Detener el bucle cuando se presione la tecla 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Liberar la cámara y cerrar todas las ventanas
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "_main_":
    # Realizar el reconocimiento facial y de ojos desde la cámara frontal
    reconocimiento_facial_y_ojos_camara()
# Proyecto: Ping Pong Clasico

## Descripción del Proyecto


## Configuración y Ejecución del Proyecto

## Librerías Utilizadas
 
## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado "Programa y Libera tu Potencial" ha sido de vital importancia en la creación y desarrollo de mi juego. Esta obra se ha convertido en una guía fundamental que me ha proporcionado un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python.

Este libro ha sido una pieza fundamental en el desarrollo de mi juego. Ha sido mi guía confiable en el proceso de aprendizaje y mi apoyo constante en la materialización de ideas. Agradezco enormemente el valioso contenido y el enfoque práctico que ha proporcionado, permitiéndome dar vida a un proyecto que me llena de orgullo y satisfacción.

Sin lugar a dudas, "Programa y Libera tu Potencial" ha sido una contribución invaluable en mi camino hacia la excelencia en la programación y en la realización de mi juego. 

 ![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)


## Agradecimientos

Queremos expresar nuestro más profundo agradecimiento a la Universidad Privada Domingo Savio por brindarnos la oportunidad de formarnos como estudiantes universitarios y permitirnos desarrollar nuestras habilidades en el apasionante mundo de la programación y el diseño de videojuegos.

 
## Cómo Contribuir
¡Únete a nuestra emocionante iniciativa "Libro: PROGRAMA Y LIBERA TU POTENCIAL" y sé parte de su mejora continua! Puedes contribuir compartiendo tus sugerencias, reportando errores, proponiendo nuevos ejercicios, compartiendo tu experiencia o proporcionando comentarios y calificaciones. ¡Tu participación marca la diferencia en este proyecto de aprendizaje y desarrollo de habilidades de programación! 

José Ignacio Arandia, Luis Andres Parada, Anderson Salvatierra y Carlos Daniel Arias y el equipo detrás de este libro agradecen tu apoyo. ¡Juntos liberemos nuestro potencial creativo a través de la codificación!

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UPDS](https://www.facebook.com/UPDS.bo)

Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)

Docente:
- [PhD.  JAIME ZAMBRANA CHACÓN](https://facebook.com/zambranachaconjaime)

Equipo de desarrollo:
- luis rodrigo aguilar justiniano
- christopher lotore herrera
- manuel paul quispe choque
- beimar andres sejas alarcon

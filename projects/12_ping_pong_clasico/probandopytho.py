import turtle
import pygame

#ventana
ventana = turtle.Screen()
ventana.title("ping pong")
ventana.bgcolor("pink")
ventana.setup(width=800,height=600)
ventana.tracer(0)

#sonido
pygame.init()
pygame.mixer.init()
pygame.mixer.music.load('PING_PONG_INTRO.mp3')
pygame.mixer.music.play(3)


#marcador
marcadorA=0
marcadorB=0

#jugadorA
jugadorA=turtle.Turtle()
jugadorA.speed(0)
jugadorA.shape("square")
jugadorA.color("blue")
jugadorA.penup()
jugadorA.goto(-350,0)
jugadorA.shapesize(stretch_wid=5, stretch_len= 1)

#jugadorB
jugadorB=turtle.Turtle()
jugadorB.speed(0)
jugadorB.shape("square")
jugadorB.color("red")
jugadorB.penup()
jugadorB.goto(350,0)
jugadorB.shapesize(stretch_wid=5, stretch_len= 1)

#pelota
pelota=turtle.Turtle()
pelota.speed(0)
pelota.shape("square")
pelota.color("green")
pelota.penup()
pelota.goto(0,0)
pelota.dx=1
pelota.dy=1

#linea divicion
division=turtle.Turtle()
division.color("white")
division.goto(0,400)
division.goto(0,-400)

#pen
pen= turtle.Turtle()
pen.speed(0)
pen.color("black")
pen.penup()
pen.hideturtle()
pen.goto(0,260)
pen.write("MarcadorA: 0          MarcadorB: 0", align="center",font=("courier", 24,"normal"))

#funciones
def jugadorA_up():
    y=jugadorA.ycor()
    y+=40
    jugadorA.sety(y)
      
def jugadorA_down():
    y=jugadorA.ycor()
    y-=40
    jugadorA.sety(y)
def jugadorB_up():
    y=jugadorB.ycor()
    y+=40
    jugadorB.sety(y)
      
def jugadorB_down():
    y=jugadorB.ycor()
    y-=40
    jugadorB.sety(y)

#teclado
ventana.listen()
ventana.onkeypress(jugadorA_up, "w")
ventana.onkeypress(jugadorA_down, "s")
ventana.onkeypress(jugadorB_up, "Up")
ventana.onkeypress(jugadorB_down, "Down")

while True:
    ventana.update()
    pelota.setx(pelota.xcor()+pelota.dx)
    pelota.sety(pelota.ycor()+pelota.dy)
    #bordes
    if pelota.ycor()>290:
        pelota.dy *=-1
    if pelota.ycor()<-290:
        pelota.dy *=-1
    #bordes izquierda/derecha
    if pelota.xcor()>390:
        pelota.goto(0,0)
        pelota.dx *=-1
        marcadorA +=1 
        pen.clear()
        pen.write("MarcadorA: {}          MarcadorB: {}".format(marcadorA,marcadorB), align="center",font=("courier", 24,"normal"))
    if pelota.xcor()< -390:
        pelota.goto(0,0)
        pelota.dx *=-1
        marcadorB +=1 
        marcadorB >=1
        pen.clear()
        pen.write("MarcadorA: {}          MarcadorB: {}".format(marcadorA,marcadorB), align="center",font=("courier", 24,"normal"))
    if ((pelota.xcor() > 340 and pelota.xcor() <350)
          and (pelota.ycor()<jugadorB.ycor()+50
          and pelota.ycor()>jugadorB.ycor()-50)):
          pelota.dx *=-1
    if ((pelota.xcor() < -340 and pelota.xcor() >-350)
          and (pelota.ycor()<jugadorA.ycor()+50
          and pelota.ycor()>jugadorA.ycor()-50)):
          pelota.dx *=-1
import yagmail

# Credenciales
username = 'admin@funcipro.edu.bo'
password = 'Clave'
server = 'smtp.gmail.com'
port = 465  # o 465 si estás utilizando SSL

# Establece conexión
yag = yagmail.SMTP(user=username, password=password, host=server, port=port)

# Correo de prueba
to = 'jameszambranachacon@gmail.com'
subject = 'Prueba'
body = 'correo_prueba. agosto 2023'

# Enviar correo
yag.send(to=to, subject=subject, contents=body)
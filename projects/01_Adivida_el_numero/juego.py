import random
import pygame

# Inicializar Pygame
pygame.init()

# Configuración de la ventana
width, height = 800, 500
ventana_principal = pygame.display.set_mode((width, height))
pygame.display.set_caption('JUEGO ADIVINA EL NÚMERO DEL 0 AL 100')

# Colores
COLOR_FONDO = (86, 113, 166)
COLOR_TEXTO = (242, 125, 22)
COLOR_TEXTO_RESULTADO = (255, 255, 255)
COLOR_BLANCO = (255, 255, 255)

# Variables para el juego
numero_aleatorio = random.randint(0, 100)
intentos = 10
texto_ingresado = ""
mensaje_a_mostrar = ""
sdff="?"

def verificar_adivinanza():
    global intentos, mensaje_a_mostrar, sdff
    valor_usuario = int(texto_ingresado)
    if intentos > 0:
        if valor_usuario == numero_aleatorio:
            mensaje_a_mostrar = f'¡Has ganado! {numero_aleatorio} es el número correcto.'
            sdff=f'{numero_aleatorio}'
        elif valor_usuario > numero_aleatorio:
            intentos -= 1
            mensaje_a_mostrar = f'El número debe ser menor. Te quedan {intentos} intentos.'
        elif valor_usuario < numero_aleatorio:
            intentos -= 1
            mensaje_a_mostrar = f'El número debe ser mayor. Te quedan {intentos} intentos.'
        else:
            mensaje_a_mostrar = '¡Algo salió mal!'
    else:
        mensaje_a_mostrar = f'¡Perdiste! el numero era:'
        sdff=f'{numero_aleatorio}'

# Crear el botón de Jugar
fuente = pygame.font.Font(None, 36)
boton_jugar = fuente.render("Jugar", True, COLOR_TEXTO)
boton_rect = boton_jugar.get_rect(center=(width // 2, 350))


fuente_solucion = pygame.font.Font('freesansbold.ttf', 150)



# Bucle principal del juego
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                verificar_adivinanza()
                texto_ingresado = ""

            elif event.key == pygame.K_BACKSPACE:
                texto_ingresado = texto_ingresado[:-1]
            else:
                texto_ingresado += event.unicode
                
        if event.type == pygame.MOUSEBUTTONDOWN:
            if boton_rect.collidepoint(event.pos):
                verificar_adivinanza()

    ventana_principal.fill(COLOR_FONDO)

    # Dibuja el texto y los elementos de la interfaz gráfica
    texto = fuente.render("Adivina el Número", True, COLOR_TEXTO)
    ventana_principal.blit(texto, (width // 2 - texto.get_width() // 2, 20))


    texto_solucion = fuente_solucion.render(sdff, True, COLOR_TEXTO)
    ventana_principal.blit(texto_solucion, (355, 50))

    # Rectángulo de entrada de texto (blanco)
    pygame.draw.rect(ventana_principal, COLOR_BLANCO, (width // 2 - 50, 180, 100, 30))
    pygame.draw.rect(ventana_principal, COLOR_TEXTO, (width // 2 - 50, 180, 100, 30), 2)

    # Dibuja el número ingresado dentro del cuadro blanco
    texto_ingresado_render = fuente.render(texto_ingresado, True, COLOR_TEXTO)
    ventana_principal.blit(texto_ingresado_render, (width // 2 - texto_ingresado_render.get_width() // 2, 180))
        
    texto_resultado = fuente.render(mensaje_a_mostrar, True, COLOR_TEXTO_RESULTADO)
    ventana_principal.blit(texto_resultado, (width // 2 - texto_resultado.get_width() // 2, 250))

    # Dibujar el botón de Jugar
    pygame.draw.rect(ventana_principal, COLOR_BLANCO, boton_rect)
    ventana_principal.blit(boton_jugar, boton_rect)

    pygame.display.flip()
# Proyecto: laberinto Desafiante
Sé parte de tu gran recuerdo, desbloquea ese niño interior y juega al famoso "Pong". ¿Cuál es la diferencia? Ya estamos grandes y ahora juega a "Pong" pero con la nueva temática de Blue Label; ¿Tiene límite de tiempo? NO, ¿Tiene límite de puntos? NO. Puedes jugar tanto y como quieras a "Pong Blue Label"; porque, este juego, es un "Elissir".
![Employee data](image/funcion.gif)
## Descripción del Proyecto

"Blue Label" es un juego que trata sobre el juego de mesa, ping pong o tenis en el cuál los oponentes deberán evitar que al momento de recibir el balón este colisiones con el borde de su lado de la pantalla. Existen 2 paletas con las cuáles tanto jugador 1 del lado izquierdo, así como jugador 2 del lado derecho deben cumplir dicho cometido de evitar que el balón colisione.

El juego pone a prueba la destreza y habilidad del jugador para evitar colisionar el balón de su lado de la pantalla. Pueden juagar tanto y como quieran porque no existe límite de tiempo ni de puntaje. Y si deseas empezar de nuevo un simple "R" en el teclado, reiniciará el puntaje para que sigan llevando la cuenta.
## Configuración y Ejecución del Proyecto
Para configurar y ejecutar el juego, sigue los siguientes pasos:

- Clona este repositorio en tu sistema local.
Asegúrate de tener Python 3.12 instalado en tu computadora.
- Instala la librería necesaria ejecutando el siguiente comando en la terminal:

   ``` pip install pygame```
- Una vez que hayas instalado pygame, ejecuta el archivo "python blue label.py" para iniciar el juego. Puedes hacerlo mediante el siguiente comando:

  ```python blue label.py```
## Librerías Utilizadas

Este proyecto utiliza las siguientes librerías Python:
- [pygame](https://www.pygame.org/) 

pygame: Es una librería de Python especializada en el desarrollo de videojuegos. Proporciona herramientas para la creación de gráficos, animaciones, sonidos y eventos, lo que la hace ideal para desarrollar juegos interactivos como "Laberinto Desafiante".
 

## Libro: PROGRAMA Y LIBERA TU POTENCIAL
El libro titulado "Programa y Libera tu Potencial" ha sido de vital importancia en la creación y desarrollo de mi juego. Esta obra se ha convertido en una guía fundamental que me ha proporcionado un sólido fundamento en la comprensión de los elementos esenciales de los algoritmos y la programación en Python.

Este libro ha sido una pieza fundamental en el desarrollo de mi juego. Ha sido mi guía confiable en el proceso de aprendizaje y mi apoyo constante en la materialización de ideas. Agradezco enormemente el valioso contenido y el enfoque práctico que ha proporcionado, permitiéndome dar vida a un proyecto que me llena de orgullo y satisfacción.

Sin lugar a dudas, "Programa y Libera tu Potencial" ha sido una contribución invaluable en mi camino hacia la excelencia en la programación y en la realización de mi juego. 

 ![Employee data](image/book.jpg)

Sobre los autores:
(http://programatupotencial.com)


## Agradecimientos

Queremos expresar nuestro más profundo agradecimiento a la Universidad Privada Domingo Savio por brindarnos la oportunidad de formarnos como estudiantes universitarios y permitirnos desarrollar nuestras habilidades en el apasionante mundo de la programación y el diseño de videojuegos.

Queremos extender nuestro reconocimiento al PhD. Jaime Zambrana Chacon, nuestro estimado docente, cuya dedicación y compromiso con la enseñanza nos han inspirado a superar desafíos y a explorar nuevas posibilidades en el desarrollo de nuestro juego.

Asimismo, agradecemos al decano de la facultad de ingeniería por fomentar un ambiente propicio para el desarrollo de nuestras capacidades y por brindarnos las herramientas necesarias para materializar nuestras ideas.

Y por supuesto, no podemos dejar de mencionar a nuestros compañeros de grupo: 

Elmer Ademar Fernandez Quispe, José Daniel Masabi Suárez, Pablo Cesar Nina Ayala, Kevin Nuñez Ortiz.

Su colaboración, trabajo en equipo y compromiso han sido clave para el éxito de este proyecto. Juntos hemos enfrentado retos, compartido conocimientos y celebrado cada avance.

Este logro es el resultado de la combinación de un excelente equipo de trabajo, el apoyo académico de nuestra universidad y el valioso acompañamiento de nuestros docentes. Gracias a esta unión de esfuerzos, hemos podido liberar nuestro potencial y llevar a cabo este emocionante proyecto.

 
## Cómo Contribuir
¡Únete a nuestra emocionante iniciativa "Libro: PROGRAMA Y LIBERA TU POTENCIAL" y sé parte de su mejora continua! Puedes contribuir compartiendo tus sugerencias, reportando errores, proponiendo nuevos ejercicios, compartiendo tu experiencia o proporcionando comentarios y calificaciones. ¡Tu participación marca la diferencia en este proyecto de aprendizaje y desarrollo de habilidades de programación! 

José Ignacio Arandia, Luis Andres Parada, Anderson Salvatierra y Carlos Daniel Arias y el equipo detrás de este libro agradecen tu apoyo. ¡Juntos liberemos nuestro potencial creativo a través de la codificación!

Para cualquier pregunta o comentario, por favor contacta al correo electrónico (sc.jaime.zambrana.c@upds.net.bo).

## Equipo de desarrollo
[UPDS](https://www.facebook.com/UPDS.bo)

Decano de la Facultad:
- [Msc. Wilmer Campos Saavedra](https://www.facebook.com/wilmercampos1)

Docente:
- [PhD.  JAIME ZAMBRANA CHACÓN](https://facebook.com/zambranachaconjaime)

Equipo de desarrollo:
- [ José Igancio Arandia Terrazas](https://www.facebook.com/jose.arandia.501151)
- [ Luis Andrés Parada Vaca](https://www.facebook.com/luisandres.paradavaca?mibextid=ZbWKwL)
- [ Anderson Salvatierra Ledezma](hhttps://www.facebook.com/iiTheBestHs?mibextid=ZbWKwL)
- [ Carlos Daniel Arias Patzy](https://www.facebook.com/carlosdaniel.ariaspatzy?mibextid=ZbWKwL)
from pyfingerprint.pyfingerprint import PyFingerprint

def read_fingerprint():
    # Intenta inicializar el sensor
    try:
        f = PyFingerprint('COM3', 57600, 0xFFFFFFFF, 0x00000000)

        if (f.verifyPassword() == False):
            raise ValueError('La contraseña del sensor de huellas dactilares es incorrecta!')

    except Exception as e:
        print('El sensor de huellas dactilares no pudo ser inicializado!')
        print('Mensaje de excepción: ' + str(e))
        exit(1)

    # Espera a que se lea una huella
    print('Esperando dedo...')
    while (f.readImage() == False):
        pass

    print('¡Huella dactilar detectada!')

if __name__ == "__main__":
    read_fingerprint()

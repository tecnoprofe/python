import pygame
import sys
import random
import os




# Inicializar pygame
pygame.init()

#MUSICA
pygame.mixer.music.load(os.path.dirname(__file__)+"\\image\\musicafondo.mp3")

pygame.mixer.music.play (1)

# Obtener la resolución de la pantalla del dispositivo
screen_info = pygame.display.Info()
screen_width = screen_info.current_w
screen_height = screen_info.current_h

# Establecer las dimensiones de la ventana del juego
window_width = screen_width
window_height = screen_height

# Crear la ventana del juego en pantalla completa
screen = pygame.display.set_mode((window_width, window_height), pygame.FULLSCREEN)

# Título de la ventana del juego
pygame.display.set_caption("Laberinto Desafiante")

# Colores
NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)
VERDE = (0, 255, 0)
NARANJA = (250, 70, 0)
FINAL = (0, 0, 250)

# Dimensiones del cuadrado (jugador)
ancho_cuadrado = 30
alto_cuadrado = 30

# Lista de barras que forman el laberinto
barras = []

# Agregar las barras que forman el laberinto
espacio_paredes = 1

# Camino horizontal superior
barras.append(pygame.Rect(0, espacio_paredes, window_width, 10))
# Camino horizontal inferior
barras.append(pygame.Rect(0, window_height - espacio_paredes - 10, window_width, 10))
# Camino vertical izquierdo
barras.append(pygame.Rect(espacio_paredes, espacio_paredes, 10, window_height - 2 * espacio_paredes))
# Camino vertical derecho
barras.append(pygame.Rect(window_width - espacio_paredes - 10, espacio_paredes, 10, window_height - 2 * espacio_paredes))

# Agregar más caminos internos
num_caminos = 5
for _ in range(num_caminos):
    camino_x = random.randint(espacio_paredes, window_width - espacio_paredes - 10)
    camino_y = random.randint(espacio_paredes, window_height - espacio_paredes - 10)
    camino_ancho = random.randint(500, 500)
    camino_alto = random.randint(10, 50)
    barras.append(pygame.Rect(camino_x, camino_y, camino_ancho, camino_alto))

# Obstáculos móviles
num_obstaculos = 70
obstaculos = []
for _ in range(num_obstaculos):
    obstaculo_x = random.randint(espacio_paredes + 10, window_width - espacio_paredes - 20)
    obstaculo_y = random.randint(espacio_paredes + 10, window_height - espacio_paredes - 20)
    obstaculo_ancho = random.randint(20, 50)
    obstaculo_alto = random.randint(20, 50)
    velocidad_x = random.choice([-2, -2, 2, 2])  # Dirección aleatoria con diferentes velocidades
    velocidad_y = random.choice([-2, -2, 2, -2])  # Dirección aleatoria con diferentes velocidades
    obstaculos.append((pygame.Rect(obstaculo_x, obstaculo_y, obstaculo_ancho, obstaculo_alto), velocidad_x, velocidad_y))

# Objetos fijos dentro del laberinto
num_objetos_fijos = 30
objetos_fijos = []
for _ in range(num_objetos_fijos):
    objeto_x = random.randint(espacio_paredes + 60, window_width - espacio_paredes - 60)
    objeto_y = random.randint(espacio_paredes + 60, window_height - espacio_paredes - 60)
    objeto_ancho = random.randint(20, 40)
    objeto_alto = random.randint(20, 40)
    objetos_fijos.append(pygame.Rect(objeto_x, objeto_y, objeto_ancho, objeto_alto))

# Definir el cuadrado (jugador) en la esquina superior izquierda, separado un poco del borde
cuadrado = pygame.Rect(60, 60, ancho_cuadrado, alto_cuadrado)

# Posición inicial del jugador (para reiniciar al tocar un objeto fijo)
posicion_inicial = cuadrado.topleft
# Cargar la imagen que aparecerá antes de iniciar el juego
imagen_inicio = pygame.image.load(os.path.dirname(__file__)+"\\image\\nombre_de_tu_imagen.png")

# Pantalla de inicio
screen.fill(NEGRO)
screen.blit(imagen_inicio, ((window_width // 2) - (imagen_inicio.get_width() // 2), (window_height // 2) - (imagen_inicio.get_height() // 2)))
pygame.display.update()

# Esperar 5 segundos antes de continuar con el juego
pygame.time.delay(5000)
 # Definir la velocidad del juego (fotogramas por segundo) 
FPS = 120
clock = pygame.time.Clock()
# Bucle principal del juego
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            
                # Limitar el bucle a la velocidad de fotogramas deseada
    clock.tick(FPS)
    for event in pygame.event.get():
        # Agregar la opción de salir del juego con el botón "Esc"
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                pygame.quit()
                sys.exit()
                 
 
    # Capturar las teclas presionadas
    teclas = pygame.key.get_pressed()
    # Mover el cuadrado con las teclas de flechas
    if teclas[pygame.K_LEFT]:
        cuadrado.move_ip(-2, 0)
    if teclas[pygame.K_RIGHT]:
        cuadrado.move_ip(2, 0)
    if teclas[pygame.K_UP]:
        cuadrado.move_ip(0, -2)
    if teclas[pygame.K_DOWN]:
        cuadrado.move_ip(0, 2)

    # Verificar si el cuadrado choca con alguna de las barras
    for barra in barras:
        if cuadrado.colliderect(barra):
            cuadrado.left = cuadrado_old.left
            cuadrado.top = cuadrado_old.top

    # Verificar si el jugador alcanzó la meta (esquina inferior derecha)
    if cuadrado.colliderect(window_width - 60, window_height - 60, 30, 30):
        #muisca de victoria
        pygame.mixer.music.load(os.path.dirname(__file__)+"\\image\\final.mp3")
        pygame.mixer.music.play(1)
    
        # Pantalla de Victoria
        screen.fill(NEGRO)
        fuente = pygame.font.SysFont(None, 100)
        mensaje = fuente.render("¡Ganaste!", True, VERDE)
        screen.blit(mensaje, ((window_width // 2) - 150, (window_height // 2) - 50))
        pygame.display.update()
        pygame.time.wait(5000)  # Esperar 2 segundos para mostrar el mensaje de victoria
        pygame.quit()
        sys.exit()

    # Verificar si el jugador toca alguno de los objetos fijos
    for objeto_fijo in objetos_fijos:
        if cuadrado.colliderect(objeto_fijo):
            # Volver al inicio (posición inicial) si se toca un objeto fijo
            cuadrado.topleft = posicion_inicial

    # Guardar la posición actual del cuadrado para el próximo ciclo
    cuadrado_old = cuadrado.copy()

    # Mover los obstáculos móviles y empujar al jugador si hay colisión
    for i, (obstaculo, velocidad_x, velocidad_y) in enumerate(obstaculos):
        obstaculo.move_ip(velocidad_x, velocidad_y)

        # Verificar si el obstáculo choca con alguna de las barras
        for barra in barras:
            if obstaculo.colliderect(barra):
                # Invertir la dirección del obstáculo al chocar con una barrera
                velocidad_x = -velocidad_x
                velocidad_y = -velocidad_y
                obstaculos[i] = (obstaculo, velocidad_x, velocidad_y)

        # Verificar si el obstáculo choca con el jugador
        if obstaculo.colliderect(cuadrado):
            # Empujar al jugador en la dirección del obstáculo
            cuadrado.move_ip(velocidad_x, velocidad_y)

    # Dibujar el laberinto (barras) en la pantalla
    screen.fill(NEGRO)
    for barra in barras:
        pygame.draw.rect(screen, BLANCO, barra)

    # Dibujar al jugador (cuadrado) en su posición actual
    pygame.draw.rect(screen, VERDE, cuadrado)

    # Dibujar los obstáculos móviles
    for obstaculo, _, _ in obstaculos:
        pygame.draw.rect(screen, BLANCO, obstaculo)

    # Dibujar los objetos fijos
    for objeto_fijo in objetos_fijos:
        pygame.draw.rect(screen, NARANJA, objeto_fijo)

    # Dibujar la meta en el laberinto (esquina inferior derecha)
    pygame.draw.rect(screen, FINAL, (window_width - 60, window_height - 60, 50, 50))
    pygame.display.update()
import turtle, time, random
posponer = 0.1

#Marcador
score = 0
high_score = 0

#Configuracion de la ventana
wn = turtle.Screen()
wn.title("juego viborita")
wn.bgcolor("grey")
wn.setup(width = 1300, height = 610)
wn.tracer(0)




#Cabeza serpiente
cabeza = turtle.Turtle()
cabeza.speed(0)
cabeza.shape("circle")
cabeza.color("dark green")
cabeza.penup()
cabeza.goto(0,0)
cabeza.direction = "stop"



#Comida
comida = turtle.Turtle()
comida.speed(0)
comida.shape("circle")
comida.color("red")
comida.penup()
comida.goto(0,100)

#Segmentos
segmentos = []

#Texto
texto = turtle.Turtle()
texto.speed(0)
texto.color("black")
texto.penup()
texto.hideturtle()
texto.goto(0,260)
texto.write("Comida: 0     Mejor puntuacion: 0", align = "center", font =("Courier", 24, "normal"))
#Funciones de la vibora
def arriba():
    cabeza.direction = "up"
def abajo():
    cabeza.direction = "down"
def izquierda():
    cabeza.direction = "left"
def derecha():
    cabeza.direction = "right"


def mov():
    if cabeza.direction == "up":
        y = cabeza.ycor()
        cabeza.sety(y + 20)
        
    if cabeza.direction == "down":
        y = cabeza.ycor()
        cabeza.sety(y - 20)

    if cabeza.direction == "left":
        x = cabeza.xcor()
        cabeza.setx(x - 20)

    if cabeza.direction == "right":
        x = cabeza.xcor()
        cabeza.setx(x + 20)

#Teclado
wn.listen()
wn.onkeypress(arriba, "w")
wn.onkeypress(abajo, "s")
wn.onkeypress(izquierda, "a")
wn.onkeypress(derecha, "d")
    
while True:
    wn.update()

    #Colisiones bordes
    if cabeza.xcor() > 620 or cabeza.xcor() < -620 or cabeza.ycor() > 280 or cabeza.ycor() < -280:
        time.sleep(1)
        cabeza.goto(0,0)
        cabeza.direction = "stop"

        #Esconder los segmentos
        for segmento in segmentos:
            segmento.goto(1000,1000)

        #Limpiar lista de segmentos
            segmento.clear()

        #Resetear marcador
        score = 0
        texto.clear()   
        texto.write("Comida: {}     Mejor puntuacion: {}".format(score, high_score), 
                align = "center", font =("Courier", 24, "normal"))

    # Cuerpo de la serpiente
    if cabeza.distance(comida) < 20: 
        x = random.randint(-280,280)
        y = random.randint(-280,280)
        comida.goto(x,y)

        nuevo_segmento = turtle.Turtle()
        nuevo_segmento.speed(0)
        nuevo_segmento.shape("circle")
        nuevo_segmento.color("light green")
        nuevo_segmento.penup()
        segmentos.append(nuevo_segmento)

        #Aumentar marcador 
        score += 1
         
        if score > high_score: 
           high_score = score 

        texto.clear()   
        texto.write("Comida: {}    Mejor puntuacion: {}".format(score, high_score), 
                align = "center", font =("Courier", 24, "normal"))
    
    

    #Mover el cuerpo de la serpiente
    totalSeg = len(segmentos)
    for index in range(totalSeg -1, 0, -1):
        x = segmentos[index - 1].xcor()
        y = segmentos[index - 1].ycor()

        segmentos[index].goto(x,y)

    if totalSeg > 0:
        x = cabeza.xcor()
        y = cabeza.ycor()
        segmentos[0].goto(x,y)

    mov()

    #Colisiones con el cuerpo
    for segmento in segmentos:
        if segmento.distance(cabeza) < 20:
            time.sleep
            cabeza.goto(0,0)
            cabeza.direction = "stop"

            #Esconder los segmentos
            for segmento in segmentos:
                segmento.goto(1000,1000)

            segmentos.clear()

            score = 0
            texto.clear()   
            texto.write("Comida: {}    Mejor puntuacion: {}".format(score, high_score), 
                align = "center", font =("Courier", 24, "normal"))
        
    time.sleep(posponer)
    

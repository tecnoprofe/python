import sys
import numpy as np
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QTableWidget, QTableWidgetItem

class AESApp(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle('AES Encryption Demonstration')
        self.resize(1024, 900)

        self.stateHex = []
        self.MatrizStateHex = np.full((4, 4), '00')

        self.keyHex = []
        self.MatrizkeyHex = np.full((4, 4), '00')

        self.s_box = np.array([
            ['63', '7C', '77', '7B', 'F2', '6B', '6F', 'C5', '30', '01', '67', '2B', 'FE', 'D7', 'AB', '76'],
            ['CA', '82', 'C9', '7D', 'FA', '59', '47', 'F0', 'AD', 'D4', 'A2', 'AF', '9C', 'A4', '72', 'C0'],
            ['B7', 'FD', '93', '26', '36', '3F', 'F7', 'CC', '34', 'A5', 'E5', 'F1', '71', 'D8', '31', '15'],
            ['04', 'C7', '23', 'C3', '18', '96', '05', '9A', '07', '12', '80', 'E2', 'EB', '27', 'B2', '75'],
            ['09', '83', '2C', '1A', '1B', '6E', '5A', 'A0', '52', '3B', 'D6', 'B3', '29', 'E3', '2F', '84'],
            ['53', 'D1', '00', 'ED', '20', 'FC', 'B1', '5B', '6A', 'CB', 'BE', '39', '4A', '4C', '58', 'CF'],
            ['D0', 'EF', 'AA', 'FB', '43', '4D', '33', '85', '45', 'F9', '02', '7F', '50', '3C', '9F', 'A8'],
            ['51', 'A3', '40', '8F', '92', '9D', '38', 'F5', 'BC', 'B6', 'DA', '21', '10', 'FF', 'F3', 'D2'],
            ['CD', '0C', '13', 'EC', '5F', '97', '44', '17', 'C4', 'A7', '7E', '3D', '64', '5D', '19', '73'],
            ['60', '81', '4F', 'DC', '22', '2A', '90', '88', '46', 'EE', 'B8', '14', 'DE', '5E', '0B', 'DB'],
            ['E0', '32', '3A', '0A', '49', '06', '24', '5C', 'C2', 'D3', 'AC', '62', '91', '95', 'E4', '79'],
            ['E7', 'C8', '37', '6D', '8D', 'D5', '4E', 'A9', '6C', '56', 'F4', 'EA', '65', '7A', 'AE', '08'],
            ['BA', '78', '25', '2E', '1C', 'A6', 'B4', 'C6', 'E8', 'DD', '74', '1F', '4B', 'BD', '8B', '8A'],
            ['70', '3E', 'B5', '66', '48', '03', 'F6', '0E', '61', '35', '57', 'B9', '86', 'C1', '1D', '9E'],
            ['E1', 'F8', '98', '11', '69', 'D9', '8E', '94', '9B', '1E', '87', 'E9', 'CE', '55', '28', 'DF'],
            ['8C', 'A1', '89', '0D', 'BF', 'E6', '42', '68', '41', '99', '2D', '0F', 'B0', '54', 'BB', '16']
        ])

        self.initUI()

    def initUI(self):
        # Primer grupo horizontal
        self.lbl_mensaje = QLabel("Mensaje: ", self)
        self.txt_mensaje = QLineEdit(self)
        self.btn_mensaje = QPushButton('Convertir a HEX', self)
        self.btn_mensaje.clicked.connect(self.btn_mensaje_clicked)
        self.txt_mensajeHex = QLineEdit(self)

        layout_mensaje = QVBoxLayout()
        layout_mensaje.addWidget(self.lbl_mensaje)
        layout_mensaje.addWidget(self.txt_mensaje)
        layout_mensaje.addWidget(self.btn_mensaje)
        layout_mensaje.addWidget(self.txt_mensajeHex)

        # Segundo grupo horizontal
        self.lbl_clave = QLabel("Clave: ", self)
        self.txt_clave = QLineEdit(self)
        self.btn_clave = QPushButton('Convertir a HEX', self)
        self.btn_clave.clicked.connect(self.btn_clave_clicked)
        self.txt_claveHex = QLineEdit(self)

        layout_clave = QVBoxLayout()
        layout_clave.addWidget(self.lbl_clave)
        layout_clave.addWidget(self.txt_clave)
        layout_clave.addWidget(self.btn_clave)
        layout_clave.addWidget(self.txt_claveHex)

        layout_men_cla = QHBoxLayout()
        layout_men_cla.addLayout(layout_mensaje)
        layout_men_cla.addLayout(layout_clave)

        # Tablas de estado
        column_width = 50  # El ancho 
        self.table1 = QTableWidget(4, 4)
        for i in range(self.table1.columnCount()):
            self.table1.setColumnWidth(i, column_width)

        self.table2 = QTableWidget(4, 4)
        for i in range(self.table2.columnCount()):
            self.table2.setColumnWidth(i, column_width)

        layout_tablas = QHBoxLayout()
        layout_tablas.addWidget(self.table1)
        layout_tablas.addWidget(self.table2)

        # Botón de Rondas
        self.lbl_rondas = QLabel("Rondas: ", self)
        self.btn_rondas = QPushButton('Ejecutar Rondas', self)
        self.btn_rondas.clicked.connect(self.btn_rondas_clicked)

        layout_rondas = QHBoxLayout()
        layout_rondas.addWidget(self.lbl_rondas)
        layout_rondas.addWidget(self.btn_rondas)

        # Tabla de resultado
        self.table3 = QTableWidget(100, 32)
        for i in range(self.table3.columnCount()):
            self.table3.setColumnWidth(i, column_width)

        layout_tablaGeneral = QHBoxLayout()
        layout_tablaGeneral.addWidget(self.table3)

        # Disposición general
        v_layout = QVBoxLayout()
        v_layout.addLayout(layout_men_cla)
        v_layout.addLayout(layout_tablas)
        v_layout.addLayout(layout_rondas)
        v_layout.addLayout(layout_tablaGeneral)
        self.setLayout(v_layout)

    def btn_mensaje_clicked(self):
        self.stateHex = [format(ord(char), '02X') for char in self.txt_mensaje.text()]
        self.txt_mensajeHex.setText(' '.join(self.stateHex))
        self.txt_mensajeHex.setReadOnly(True)

        for i in range(min(16, len(self.stateHex))):
            self.MatrizStateHex[i // 4, i % 4] = self.stateHex[i]

        for row in range(self.MatrizStateHex.shape[0]):
            for col in range(self.MatrizStateHex.shape[1]):
                self.table1.setItem(row, col, QTableWidgetItem(str(self.MatrizStateHex[row, col])))

    def btn_clave_clicked(self):
        self.keyHex = [format(ord(char), '02X') for char in self.txt_clave.text()]
        self.txt_claveHex.setText(' '.join(self.keyHex))
        self.txt_claveHex.setReadOnly(True)

        for i in range(min(16, len(self.keyHex))):
            self.MatrizkeyHex[i // 4, i % 4] = self.keyHex[i]

        for row in range(self.MatrizkeyHex.shape[0]):
            for col in range(self.MatrizkeyHex.shape[1]):
                self.table2.setItem(row, col, QTableWidgetItem(str(self.MatrizkeyHex[row, col])))

    def btn_rondas_clicked(self):
        # AddRoundKey (XOR)
        matriz1_bin = np.vectorize(lambda x: int(x, 16))(self.MatrizStateHex)
        matriz2_bin = np.vectorize(lambda x: int(x, 16))(self.MatrizkeyHex)
        result_matrix = np.bitwise_xor(matriz1_bin, matriz2_bin)
        result_matrix_hex = np.vectorize(lambda x: format(x, '02X'))(result_matrix)

        # Mostrar resultado AddRoundKey
        for i in range(result_matrix_hex.shape[0]):
            for j in range(result_matrix_hex.shape[1]):
                self.table3.setItem(i, j, QTableWidgetItem(str(result_matrix_hex[i, j])))

        # SubBytes
        result_matrix_subbytes = np.vectorize(lambda x: self.s_box[int(x[0], 16)][int(x[1], 16)])(result_matrix_hex)

        # Mostrar resultado SubBytes
        for i in range(result_matrix_subbytes.shape[0]):
            for j in range(result_matrix_subbytes.shape[1]):
                self.table3.setItem(i+ 5 , j, QTableWidgetItem(str(result_matrix_subbytes[i, j])))

        # ShiftRows
        result_matrix_shiftrows = np.copy(result_matrix_subbytes)
        result_matrix_shiftrows[1] = np.roll(result_matrix_shiftrows[1], -1)
        result_matrix_shiftrows[2] = np.roll(result_matrix_shiftrows[2], -2)
        result_matrix_shiftrows[3] = np.roll(result_matrix_shiftrows[3], -3)

        # Mostrar resultado ShiftRows
        for i in range(result_matrix_shiftrows.shape[0]):
            for j in range(result_matrix_shiftrows.shape[1]):
                self.table3.setItem(i + 5, j+5, QTableWidgetItem(str(result_matrix_shiftrows[i, j])))

        # MixColumns
        def gmul(a, b):
            p = 0
            for _ in range(8):
                if b & 1:
                    p ^= a
                hi_bit_set = a & 0x80
                a <<= 1
                if hi_bit_set:
                    a ^= 0x1b
                b >>= 1
            return p

        def mix_single_column(a):
            return [
                gmul(a[0], 2) ^ gmul(a[1], 3) ^ gmul(a[2], 1) ^ gmul(a[3], 1),
                gmul(a[0], 1) ^ gmul(a[1], 2) ^ gmul(a[2], 3) ^ gmul(a[3], 1),
                gmul(a[0], 1) ^ gmul(a[1], 1) ^ gmul(a[2], 2) ^ gmul(a[3], 3),
                gmul(a[0], 3) ^ gmul(a[1], 1) ^ gmul(a[2], 1) ^ gmul(a[3], 2)
            ]

        def mix_columns(state):
            for i in range(4):
                column = [state[j][i] for j in range(4)]
                mixed_column = mix_single_column(column)
                for j in range(4):
                    state[j][i] = mixed_column[j]
            return state

        # Convertir a binario para el cálculo de mix columns
        state_bin = np.vectorize(lambda x: int(x, 16))(result_matrix_shiftrows)

        # Aplicar mix columns
        mixed_state = mix_columns(state_bin)

        # Convertir de vuelta a hexadecimal para mostrar
        mixed_state_hex = np.vectorize(lambda x: format(x, '02X'))(mixed_state)

        # Mostrar resultado MixColumns
        for i in range(mixed_state_hex.shape[0]):
            for j in range(mixed_state_hex.shape[1]):
                self.table3.setItem(i + 5, j+10, QTableWidgetItem(str(mixed_state_hex[i, j])))        

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = AESApp()
    window.show()
    sys.exit(app.exec_())

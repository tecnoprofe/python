import tkinter as tk
from tkinter import ttk
import numpy as np
# Crear una matriz vacía de 4x4 con dtype=object



mensajehex=""
llavehex=""

#----------------------------------------------------------------------------
# funcion para convertir de string a un hexadecimal pero en formato vector.
def string_to_hex(cadena):    
    hex_values = []
    # Iterar a través de cada carácter en la cadena
    for char in cadena:
        # Convertir el carácter a su valor hexadecimal
        hex_value = format(ord(char), '02x')
        # Agregar el valor hexadecimal a la lista
        hex_values.append(hex_value)

    # Unir los valores hexadecimales en una cadena y devolver
    return hex_values
    #return ''.join(hex_values)
#----------------------------------------------------------------------------


# Función que se activa al presionar el botón
def on_button_click():
    # rellenamos con ceros hexadecimales
    arreglo_con_relleno = string_to_hex(mensaje.get()) + ['0x00'] * (16 - len(string_to_hex(mensaje.get())))
    # Agregar ceros al final hasta tener 16 elementos
    arreglo_con_relleno = (np.array(arreglo_con_relleno).reshape(4, 4))
    print("su mensaje en HEX es: ", ''.join(string_to_hex(mensaje.get())))


   


    # rellenamos con ceros hexadecimales
    llave_con_relleno = string_to_hex(llave.get()) + ['0x00'] * (16 - len(string_to_hex(llave.get())))
    # Agregar ceros al final hasta tener 16 elementos
    print(np.array(llave_con_relleno).reshape(4, 4))
    print("su clave en HEX es: ", ''.join(string_to_hex(mensaje.get())))

    # Botón para limpiar el Treeview
    btn_clear = tk.Button(root, text="Limpiar Treeview", command=lambda: clear_treeview(tree))
    btn_clear.pack(pady=20)

def btn_mensaje():
    print("fdsfsdf")

#----------------------------------------------------------------------------
# FUNCION PRINCIPAL 
# Crear la ventana principal
def main():    
    root = tk.Tk()
    root.title("Encriptación AES")
    root.geometry("600x500") # Anchura x Altura    

    # Añadir una etiqueta (label) a la ventana
    labelmensaje = tk.Label(root, text="Esta es una ventana personalizada con tkinter.")
    labelmensaje.pack(pady=20)
    # Añadir un botón a la ventana
    btnmensaje = tk.Button(root, text="Convertir a HEX", command=btn_mensaje)
    btnmensaje.pack(pady=20)

    # Añadir una etiqueta (label) instructiva
    label1 = tk.Label(root, text="Ingrese mensaje a encriptar:")
    label1.pack(pady=10)

    # Añadir la caja de texto (Entry)
    mensaje = tk.Entry(root, width=30)
    mensaje.pack(pady=10)

    # Añadir una etiqueta (label) instructiva
    label2 = tk.Label(root, text="Ingrese llave a encriptar:")
    label2.pack(pady=10)

    # Añadir la caja de texto (Entry)
    llave = tk.Entry(root, width=30)
    llave.pack(pady=10)    

    # Ejecutar el bucle principal
    root.mainloop()

if __name__ == "__main__":
    main()
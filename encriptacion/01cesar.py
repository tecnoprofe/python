def caesar_cipher(text, shift):
    result = ""

    # Recorrer cada caracter del texto
    for char in text:
        # Cifrar caracteres alfabéticos
        if char.isalpha():
            shifted = ord(char) + shift
            if char.islower():
                if shifted > ord('z'):
                    shifted -= 26
                elif shifted < ord('a'):
                    shifted += 26
            elif char.isupper():
                if shifted > ord('Z'):
                    shifted -= 26
                elif shifted < ord('A'):
                    shifted += 26
            result += chr(shifted)
        else:
            # Añadir caracteres no alfabéticos sin cambios
            result += char

    return result

text = "Jaime Zambrana Chacon!"
shift = 3  # Puedes cambiar el valor para cifrar

encrypted = caesar_cipher(text, shift)
print(f"Encriptado: {encrypted}")

decrypted = caesar_cipher(encrypted, -shift)
print(f"Desencriptado: {decrypted}")
def mod_inverse_iterative(e, phi):
    """
    Calcula el inverso modular de e con respecto a phi usando un enfoque iterativo.
    Devuelve el inverso modular d.
    """
    t, new_t = 0, 1
    r, new_r = phi, e
    
    while new_r != 0:
        quotient = r // new_r
        t, new_t = new_t, t - quotient * new_t
        r, new_r = new_r, r - quotient * new_r
    
    if r > 1:
        raise Exception('El inverso modular no existe')
    if t < 0:
        t += phi
    
    return t

# Ejemplo de uso:
e = 313
phi = 101160660

d = mod_inverse_iterative(e, phi)
print(f"El inverso modular de {e} modulo {phi} es: {d}")

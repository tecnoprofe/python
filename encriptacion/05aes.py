# Instalar libreria de encriptacion previamente. 
# pip install pycryptodome

import base64
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from Crypto.Random import get_random_bytes
import tkinter as tk

def cifrar_aes(texto, clave):
    # Generamos un Vector de Inicialización (IV) aleatorio de 16 bytes.
    iv = get_random_bytes(16)

    # Creamos el objeto 'cipher' usando la clave, el modo CBC y el IV.
    cipher = AES.new(clave, AES.MODE_CBC, iv)

    # Convertimos el texto a bytes y lo ajustamos (padding) para que su longitud sea múltiplo del tamaño de bloque de AES.
    texto_bytes = pad(texto.encode('utf-8'), AES.block_size)

    # Ciframos el texto ajustado.
    ciphertext = cipher.encrypt(texto_bytes)

    # Convertimos el texto cifrado a base64 para que sea legible y fácil de transmitir.
    encoded_ciphertext = base64.b64encode(ciphertext)
    
    return encoded_ciphertext

# Definimos una clave de ejemplo de 128 bits.
clave = b"1234567812345678"

# Definimos el texto que queremos cifrar.
texto = "hola"

# Llamamos a la función de cifrado.
resultado = cifrar_aes(texto, clave)

# Mostramos el resultado.
print("Texto cifrado:", resultado.decode('utf-8'))
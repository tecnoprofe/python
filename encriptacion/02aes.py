import sys
from PyQt5.QtWidgets import QApplication, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout, QWidget

class MyApp(QWidget):
    def __init__(self):
        super().__init__()

        # Primer grupo
        label1 = QLabel('Label 1:')
        textbox1 = QLineEdit()

        vbox1 = QVBoxLayout()  # Layout vertical para el primer grupo
        vbox1.addWidget(label1)
        vbox1.addWidget(textbox1)

        # Segundo grupo
        label2 = QLabel('Label 2:')
        textbox2 = QLineEdit()

        vbox2 = QVBoxLayout()  # Layout vertical para el segundo grupo
        vbox2.addWidget(label2)
        vbox2.addWidget(textbox2)

        # Layout principal horizontal para organizar ambos grupos horizontalmente
        hbox = QHBoxLayout()
        hbox.addLayout(vbox1)
        hbox.addLayout(vbox2)

        self.setLayout(hbox)

app = QApplication(sys.argv)
window = MyApp()
window.show()
sys.exit(app.exec_())

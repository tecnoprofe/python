import numpy as np

# Create two 4x4 matrices with hexadecimal values
matrix1 = np.array([
    ['0a', '0b', '0c','55'],
    ['0a', '0b', '0c','55'],
    ['0a', '0b', '0c','55'],
    ['0a', '0b', '0c','55']
], dtype=np.uint8)

matrix2 = np.array([
    ['0a', '0b', '0c','55'],
    ['0a', '0b', '0c','55'],
    ['0a', 'ab', 'dc','54'],
    ['0a', '0b', '0c','55']
], dtype=np.uint8)

# Apply bitwise XOR operation
result = np.bitwise_xor(matrix1, matrix2)

# Convert to hexadecimal format for visualization
hex_result = np.vectorize(hex)(result)

print(hex_result)
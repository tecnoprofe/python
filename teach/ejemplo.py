import pygame

pygame.init() 
pantalla=pygame.display.set_mode((800,500))
x=500
y=40
incremento=10
watch=pygame.time.Clock()

BLACK=(0,0,0)
WHITE=(255,255,255)
RED=(255,0,0)
GREEN=(0,255,0)
BLUE=(0,0,255)
YELLOW=(255,255,0)
DARK_RED=(139,0,0)
LIGHT_BLUE = (102,255,255)
CORAL=(255,127,80)
PINK=(255,5,228)
PURPLE = (171, 0, 255)    
light_green=(25, 226, 150)
GOLDEN =(43, 233 ,15)
teal=(0,128,128)
colorG1=(0,153,205)
MAGENTA=(255,0,255)
GOLDEN_ROD=(218,165,0)
PINK_PANTHER=(220,96,194)

ancho=40
alto=40
cuadrado=pygame.Rect(x,y,ancho,alto)
barra1=pygame.Rect(100,0,40,600)

fondo=pygame.image.load("fondoestrellado01.png")
fondo_rect=fondo.get_rect()


meteorito=pygame.image.load("meteorito20.png")
meteorito_rect=meteorito.get_rect()

velocidad=0
altura_salto=10
salto=False


meteorito_rect.left=400
meteorito_rect.top=500-62

# ========================================
# Juego Bucle
while (1==1):
    watch.tick(15)
    for evento in pygame.event.get():        
        # ========================================
        # control de salida del programa
        if evento.type==pygame.QUIT:
            quit()
    # ========================================
    # LOGICA
    if evento.type==pygame.KEYDOWN:                
        if evento.key==pygame.K_LEFT:
            velocidad=-9        
            meteorito_rect.left+=velocidad
            print("izz")
        if evento.key==pygame.K_RIGHT:
            velocidad=9
            meteorito_rect.left+=velocidad
            print("der")
        if evento.key==pygame.K_UP:
            print("SALTANDOOO")
            salto=True

    if evento.type==pygame.K_UP:
        if evento.key==pygame.K_LEFT:
            velocidad=0
            meteorito_rect.left+=velocidad
        if evento.key==pygame.K_RIGHT:
            velocidad=0
            meteorito_rect.left+=velocidad
    
    if salto==True:        
        if altura_salto>=-10:
            meteorito_rect.top-=(altura_salto*abs(altura_salto))*0.5
            altura_salto-=1            
        else:            
            altura_salto=10          
            salto=False


        
    # ========================================
    # DIBUJAR EN PYGAME
    
    
    pantalla.blit(fondo,fondo_rect)
    pantalla.blit(meteorito,meteorito_rect)        

    pygame.display.flip()
    
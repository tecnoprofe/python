import tkinter as tk
from tkinter import messagebox

ventana=tk.Tk() 
ventana.title("Ejmplo caculadora")
ventana.geometry("500x300")

etiqueta1= tk.Label(ventana,text="introduce un numero")
etiqueta1.pack(pady=10)

caja1= tk.Entry(ventana)
caja1.pack(pady=10)

etiqueta2= tk.Label(ventana,text="Introduce otro numero")
etiqueta2.pack(pady=10)

caja2= tk.Entry(ventana)
caja2.pack(pady=10)

def calcular():
    a = caja1.get()
    b = caja2.get()
    suma = int(a) + int(b)
    messagebox.showinfo("resultado",suma)

boton = tk.Button(ventana,text="Ingresar",command=calcular)
boton.pack(pady=10)

ventana.mainloop()
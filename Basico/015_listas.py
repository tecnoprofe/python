import tkinter as tk
from tkinter import messagebox

# Crear la ventana principal
root = tk.Tk()
root.title("Gestión de Lista con Tkinter")
root.geometry("500x300")

# Lista para almacenar los elementos
elementos = []

# Función para agregar un elemento a la lista
def agregar_elemento():
    elemento = entry_elemento.get()
    if elemento:
        elementos.append(elemento)
        actualizar_lista()
        entry_elemento.delete(0, tk.END)
    else:
        messagebox.showwarning("Advertencia", "Por favor, ingrese un elemento.")

# Función para eliminar el elemento seleccionado de la lista
def eliminar_elemento():
    seleccion = listbox_elementos.curselection()
    if seleccion:
        elemento = listbox_elementos.get(seleccion)
        elementos.remove(elemento)
        actualizar_lista()
    else:
        messagebox.showwarning("Advertencia", "Por favor, seleccione un elemento para eliminar.")

# Función para actualizar la lista mostrada en el listbox
def actualizar_lista():
    listbox_elementos.delete(0, tk.END)
    for elemento in elementos:
        listbox_elementos.insert(tk.END, elemento)

# Crear una etiqueta (Label) y una caja de texto (Entry) para ingresar elementos
label_elemento = tk.Label(root, text="Ingrese un elemento:")
label_elemento.pack(pady=5)
entry_elemento = tk.Entry(root)
entry_elemento.pack(pady=5)

# Crear un botón para agregar el elemento a la lista
button_agregar = tk.Button(root, text="Agregar", command=agregar_elemento)
button_agregar.pack(pady=5)

# Crear un listbox para mostrar los elementos de la lista
listbox_elementos = tk.Listbox(root)
listbox_elementos.pack(pady=10, fill=tk.BOTH, expand=True)

# Crear un botón para eliminar el elemento seleccionado de la lista
button_eliminar = tk.Button(root, text="Eliminar", command=eliminar_elemento)
button_eliminar.pack(pady=5)

# Ejecutar el bucle principal de la aplicación
root.mainloop()

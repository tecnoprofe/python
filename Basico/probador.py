# Importamos la biblioteca NumPy.
import numpy as np

# Creamos una matriz 3x3 con valores específicos.
matriz = np.array([[3, 1, 4], 
                   [2, 5, 1], 
                   [6, 2, 9]])

# Usamos la función np.sort() para ordenar la matriz a lo largo del eje 1 (las filas). 
# Esto ordena cada fila individualmente en orden ascendente.
matriz_ordenada_ascendente = np.sort(matriz, axis=1)

# Para ordenar en orden descendente, primero ordenamos 
# en orden ascendente y luego invertimos el orden 
# de los elementos en cada fila.
# La notación [:, ::-1] invierte el orden de los elementos en cada fila 
# (el segundo argumento de ::-1 especifica el paso como -1, lo que invierte el orden).
matriz_ordenada_descendente = np.sort(matriz, axis=1)[:, ::-1]

# Imprimimos la matriz ordenada en orden ascendente.
print("La matriz ordenada en orden ascendente es:")
print(matriz_ordenada_ascendente)
import tkinter as tk
from tkinter import messagebox
import math

ventana=tk.Tk() 
ventana.title("Ecuacion 2 grado")
ventana.geometry("500x300")

# esto crea un etiqueta
etiquetaA=tk.Label(ventana,text="A")
etiquetaA.pack(pady=1)

# esto crea una caja de texto que se llama Entry
caja1=tk.Entry(ventana)
caja1.pack(pady=1)

etiquetaB=tk.Label(ventana,text="B")
etiquetaB.pack(pady=1)

caja2=tk.Entry(ventana)
caja2.pack(pady=1)

etiquetaC=tk.Label(ventana,text="C")
etiquetaC.pack(pady=1)

caja3=tk.Entry(ventana)
caja3.pack(pady=1)

def ecuacion2():
    a=int(caja1.get())
    b=int(caja2.get())
    c=int(caja3.get())
    discriminante=b*b-4*a*c
    if discriminante < 0:
        messagebox.showinfo("error",'NO hay soluciones negativas '+str(discriminante))
    else:
        x1=round((-b+math.sqrt(discriminante))/(2*a),1)
        x2=round((-b-math.sqrt(discriminante))/(2*a),1)
        messagebox.showinfo("Solucion",'x1='+str(x1)+' x2='+str(x2))

    
boton=tk.Button(ventana,text="Encontrar Raices",command=ecuacion2)
boton.pack(pady=3)

ventana.mainloop()
